/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe Settings
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.util;

//Java imports
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.util.Properties;

public abstract class Settings {

    protected String settingsFile;
    protected Properties settings = new Properties();
    
    public Settings(String arq){
        settingsFile = arq;
        
        loadSettings();
    }

    public void loadSettings() {
        
      System.out.print("Settings::loadSettings (" + settingsFile + ") --> ");

      File file = new File(settingsFile);

        FileInputStream fis = null;
        settings.clear();

        // abre o arquivo de configs
        try {
            fis = new FileInputStream(file);
            settings.load(fis);
            fis.close();
        } catch (IOException ex) {
            System.out.println("Erro em Settings::load.");
            ex.printStackTrace();
        }

        // leitura das configurações
        loadStrings();
        
        System.out.println("Sucesso!");
    }

    public void saveSettings() {
                
        String newFile = settingsFile.replace(".settings", "_saved.settings");  
        System.out.print("Settings::saveSettings ("+ newFile +")--> ");

        File file = new File(newFile);

        FileOutputStream fos = null;
        settings.clear();
        
        // escrita das configurações
        saveStrings();

        // abre o arquivo de configs
        try {
            fos = new FileOutputStream(file);
            settings.store(fos, "Configurações salvas pelo software.");
            fos.close();
            
        } catch (IOException ex) {
            System.out.println( "Erro em Settings::write." );
            ex.printStackTrace();
        } 
        
        System.out.println("Sucesso!");
    }
    
    protected void clearSettings() {
        settings.clear();
    }

    protected abstract void loadStrings();

    protected abstract void saveStrings();
}
