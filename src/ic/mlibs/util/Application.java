/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe Application
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.util;

public interface Application {
    
    // Liga a aplicação
    void create() throws Exception;
    
    // Libera os recursos da aplicação
    void destroy();
    
    // Executa a aplicação
    void run();
    
    // Display da aplicação 
    void render();

    // Reset da aplicaçao
    void reset();
    
    // Reload das propriedades
    void reload();
}
