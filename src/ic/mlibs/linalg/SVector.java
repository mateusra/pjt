/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe SVector
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.linalg;

// MTJ includes
import ic.mlibs.util.Helper;
import no.uib.cipr.matrix.Vector.Norm;
import no.uib.cipr.matrix.sparse.SparseVector;

public class SVector {
    
    private SparseVector vec = null;

    
    public SVector(int n){
        vec = new SparseVector(n);
    }
    
    public SVector(int n, int nz) {
        vec = new SparseVector(n,nz);
    }
    
    public SVector(SparseVector v) {
        vec = v.copy();
    }
    
    //-------
    
    public SparseVector getVec() {
        return vec;
    }

    public void setVec(SparseVector vec) {
        this.vec = vec;
    }
    
    //-------

    /**
     * Acesso as coordenadas do vetor
     * 
     * @return O array das coordenadas
     */
    public double[] getVecData() {
        return vec.getData() ;
    }

    /**
     * Acesso as coordenadas do vetor
     * 
     * @return O array das coordenadas
     */
    public double getVecData(int i) {
        return (double) vec.get(i);
    }

    /**
     * Atribui as coordenadas do vetor
     * 
     * @param c O array das coordenadas
     */
    public void setVecData(int n, int [] index, double [] c) {
        double [] dat = c;
        vec = new SparseVector(n, index, dat) ;
    }

    /**
     * Atribui as coordenadas do vetor
     * 
     * @param c O array das coordenadas
     */
    public void setVecData(double c, int i) {
        vec.set(i, c);
    }
        
    //----- Operações
            
    public SVector opAdd (SVector p) {
        
        SparseVector   op = new SparseVector( vec );
        SparseVector dres = (SparseVector) op.add( p.vec );
        
        return new SVector( dres );
    }
    
    public SVector opSub (SVector p) {
        
        SparseVector   op = new SparseVector( vec );
        SparseVector dres = (SparseVector) op.add(-1.0, p.vec );
        
        return new SVector( dres );
    }
    
    public double dot (SVector p) {
                
        return (double) vec.dot( p.vec );
    }
    
    public double size() {
                
        return (double) vec.norm(Norm.Two) ;
    }

}
