/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe DMatrix
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.linalg;

// MTJ includes
import ic.mlibs.util.Helper;
import no.uib.cipr.matrix.EVD;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.NotConvergedException;

public class DMatrix {

    private DenseMatrix mat = null;;

    /**
     * Construtor Default
     */
    public DMatrix(int n){
        mat = new DenseMatrix(n,n) ;
    }
    
    /**
     * Construtor Default
     */
    public DMatrix(int n, int m){
        mat = new DenseMatrix(n,m);
    }

    /**
     * Cria uma nova matriz usando 1 arrays de doubles
     * 
     * @param p Array com a posição
     */
    public DMatrix(double [][] m) {
        mat = new DenseMatrix( m );
    }

    //------
    
    public DenseMatrix getMat() {
        return mat;
    }

    public void setMat(DenseMatrix mat) {
        this.mat = mat;
    }
    
    //------ Acesso e Atribuição

    public double [] getMatData() {
        return mat.getData();
    }

    public double getMatData(int i, int j) {
        if( i<mat.numRows() && j<mat.numColumns() ) return (double) mat.get(i,j);
        else return 0; 
    }

    public void setMatData(double [][] data) {
        mat = new DenseMatrix( data );
    }

    public void setMatData(double data, int i, int j) {
        mat.set(j, j, data);
    }
    
    //------- Linear Algebra
    
    public DVector solve( DVector b ) {
        
        DenseVector auxX = new DenseVector( mat.numColumns() );       
        auxX = (DenseVector) mat.solve( b.getVec(),auxX );

        return new DVector( auxX.getData() ) ;
    }

    public double[] realEigenvalues() throws NotConvergedException  {
        
        if( !mat.isSquare() ) return null;
        
        EVD eingen = EVD.factorize( mat );      
        double[] d = eingen.getRealEigenvalues();
        
        return d;
    }
}
