/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe SMatrix
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.linalg;


//MTJ imports
import no.uib.cipr.matrix.sparse.*;

public class SMatrix {
    
    private FlexCompRowMatrix mat = null;
       
    public SMatrix(int n, int m){
        mat = new FlexCompRowMatrix(n,m);
    }


    //------ Acesso e Atribuição

    public double getMatData(int i, int j) {
        return (double) mat.get(i,j);
    }
    
    public void getRowData(int i, int ids[], double [] data) {
        SparseVector vec = mat.getRow(i);
        
        ids = vec.getIndex();
        data= vec.getData() ;
    }

    public void setMatData(double data, int i, int j) {
        mat.set(i, j, data);
    }
    
    public void setRowData(int n, int[] ids, double [] data, int i) {
        SparseVector vec = new SparseVector( n, ids, data );
        
        mat.setRow(i, vec);
    }
    
    //------- Linear Algebra
    
    public SVector solve(SVector b, SVector g, String sStr) {

        IterativeSolver solver = null;
        SparseVector auxB = new SparseVector( b.getVec() );
        SparseVector auxX = new SparseVector( g.getVec() );

        if (sStr.equalsIgnoreCase("QMR")) {
            solver = new QMR(auxX);
        } else if (sStr.equalsIgnoreCase("CG")) {
            solver = new CG(auxX);
        } else if (sStr.equalsIgnoreCase("CGS")) {
            solver = new CGS(auxX);
        } else if (sStr.equalsIgnoreCase("GMRES")) {
            solver = new GMRES(auxX);
        } else if (sStr.equalsIgnoreCase("BiCG")) {
            solver = new BiCG(auxX);
        } else if (sStr.equalsIgnoreCase("BiCGstab")) {
            solver = new BiCGstab(auxX);
        } else if (sStr.equalsIgnoreCase("IR")) {
            solver = new IR(auxX);
        }
        
        Preconditioner M = solver.getPreconditioner();
        M = new DiagonalPreconditioner( mat.numRows() );
        
        try {
            solver.solve(mat, auxB, auxX);
        } catch (IterativeSolverNotConvergedException ex) {
            System.out.println( ex.getIterations() );
            System.out.println( ex.getReason() );
            System.out.println( ex.getResidual() );
        }

        return new SVector( auxX );
    }
}
