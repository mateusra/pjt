/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe DVector
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.linalg;

import ic.mlibs.util.Helper;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.Vector.Norm;

public class DVector {
    
    private DenseVector vec = null;
   
    /**
     * Construtor Default
     */
    public DVector(int n){
        vec = new DenseVector(n);
    }

    /**
     * Cria um novo vetor usando 1 arrays de doubles
     * 
     * @param p Array com a posição
     */
    public DVector(double [] p) {
        vec = new DenseVector( p );
    }
    
    /**
     * Cria um novo vetor usando 3 doubles
     * 
     * @param x Posição em X
     * @param y Posição em Y
     * @param z Posição em Z
     */
    public DVector(double x, double y, double z) {
        double [] coords = new double[3];
        
        coords[0] = x; 
        coords[1] = y;
        coords[2] = z;
        
        vec = new DenseVector( coords );
    }   
    
    //-------

    public DenseVector getVec() {
        return vec;
    }

    public void setVec(DenseVector vec) {
        this.vec = vec;
    }
    
    //-------
    
    /**
     * Acesso as coordenadas do vetor
     * 
     * @return O array das coordenadas
     */
    public double[] getVecData() {
        return vec.getData();
    }

    /**
     * Acesso as coordenadas do vetor
     * 
     * @return O array das coordenadas
     */
    public double getVecData(int i) {
        if( i<vec.size() ) return (double) vec.get(i);
        else return 0;
    }

    /**
     * Atribui as coordenadas do vetor
     * 
     * @param c O array das coordenadas
     */
    public void setVecData(double[] c) {
        vec = new DenseVector( c ) ;
    }

    /**
     * Atribui as coordenadas do vetor
     * 
     * @param c O array das coordenadas
     */
    public void setVecData(double c, int i) {
        vec.set(i, c);
    }

        
    //----- Operações
        
    public DVector opAdd (DVector p) {
        
        DenseVector   op = new DenseVector( vec );
        DenseVector dres = (DenseVector) op.add( p.vec );
        
        return new DVector( dres.getData() );
    }
    
    public DVector opSub (DVector p) {
        
        DenseVector   op = new DenseVector( vec );
        DenseVector dres = (DenseVector) op.add(-1.0, p.vec );
        
        return new DVector( dres.getData() );
    }
         
    public DVector opScale (float p) {
        
        DenseVector   op = new DenseVector( vec );
        DenseVector dres = (DenseVector) op.scale( p );
        
        return new DVector( dres.getData() );
    }
   
    public double dot (DVector p) {
                
        return (double) vec.dot( p.vec );
    }
    
    public double size() {
                
        return (double) vec.norm(Norm.Two) ;
    }
    
    public double normalize() {
        
        double sz = size();
        if(sz < Double.MIN_VALUE) sz = Double.MIN_VALUE;       
               
        vec.scale(1.0 / sz) ;
        
        return sz;
    }   
    
    public double norm() {
        
        double sz = size();
        if(sz < Double.MIN_VALUE) sz = Double.MIN_VALUE;       
        
        return sz;
    }   
}
