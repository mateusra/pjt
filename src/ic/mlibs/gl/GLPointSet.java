/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe GLPointSet
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.gl;

//LWJGL estáticos
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;

//LWJGL
import org.lwjgl.util.glu.Sphere;

//Meus imports
import ic.mlibs.structures.Point3D;
import ic.mlibs.structures.PointSet;
import ic.mlibs.util.ColorMap;

public class GLPointSet extends PointSet implements GLObject{
  
    private ColorMap map = new ColorMap();

    @Override
    public void render(String cmap, String rmode, double pmin, double pmax) {
        // escolhe o mapa de cores
        map.setColorMap(cmap);
        // seta os valores mínimo e máximo para o mapa
        map.setMapExtrems(pmin, pmax);

        // escolhe o que exibir
        if (rmode.contains("Points")) {
            drawVerts();

        } else if (rmode.contains("Spheres")) {
            drawSpheres();
        }
                
        // altera o max e min
        map.setMapExtrems(0, 255);
        // desenha a palheta de cores
        map.drawMap(-1.25f,-1.25f,0.1f,2.5f);
    }

    //---
    
    private void drawVerts() {

        glPointSize(8);
        glBegin(GL_POINTS);
        {
            for (int i = 0; i < getNvert(); i++) {
                Point3D v = getG(i);

                // cor do vértice
                map.setGLColor((Double) v.getObj(), (byte) 255, true);
                // normal do vértice
                glNormal3d(v.getNrmX(), v.getNrmY(), v.getNrmZ());
                // posição do vértice
                glVertex3d(v.getPosX(), v.getPosY(), v.getPosZ());
            }
        }
        glEnd();
        glPointSize(1);
    }

    private void drawSpheres() {

        //Cria a esfera
        Sphere s = new Sphere();
        //Render setup
        s.setDrawStyle(GLU_FILL);      
        s.setNormals(GLU_SMOOTH);
        
        for (int i = 0; i < getNvert(); i++) {
            Point3D v = getG(i);
            
            //Posiciona a esfera
            glPushMatrix();
            glTranslated(v.getPosX(), v.getPosY(), v.getPosZ());

            // cor do vértice
            map.setGLColor((Double) v.getObj(), (byte) 255, true);
            //s.draw( (Double)v.getObj(), 5, 5);
            s.draw( 0.03f, 5, 5);
            
            //Retorna a matriz
            glPopMatrix();
          }
     }
}
