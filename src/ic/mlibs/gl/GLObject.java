
/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Interface GLObject
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.gl;

public interface GLObject {
        
    //Método de render
    public void render(String cmap, String rmode, double pmin, double pmax);
}
