/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe Point3D
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.structures;

import ic.mlibs.linalg.DVector;

public class Point3D {

    private DVector pos = null;
    private DVector nrm = null;
    
    private Object  obj = null;
    
    //-------
    
    public Point3D(){
        pos = new DVector(3);
        nrm = new DVector(3);
    }

    /**
     * Cria um novo ponto usando 3 doubles
     * 
     * @param x Posição em X
     * @param y Posição em Y
     * @param z Posição em Z
     */
    public Point3D(double x, double y, double z) {
        pos = new DVector(x,y,z);
        nrm = new DVector(3);
    }    
    
    /**
     * Cria um novo ponto usando 3 doubles
     * 
     * @param x  Posição em X
     * @param y  Posição em Y
     * @param z  Posição em Z
     * @param nx Posição em NX
     * @param ny Posição em NY
     * @param nz Posição em NZ
     */
    public Point3D(double x, double y, double z, double nx, double ny, double nz) {
        pos = new DVector( x, y, z);
        nrm = new DVector(nx,ny,nz);
    }    
    
    /**
     * Cria um novo ponto usando 2 arrays de doubles
     * 
     * @param p Array com a posição
     * @param n Array com a normal
     */
    public Point3D(double [] p, double [] n) {
        pos = new DVector(p);
        nrm = new DVector(n);
    }

    /**
     * Cria um novo ponto usando 1 arrays de doubles
     * 
     * @param p Array com a posição
     */
    public Point3D(double [] p) {
        pos = new DVector(p);
        nrm = new DVector(3);
    }
    
    //-------

    /**
     * Acesso à posição do ponto
     * 
     * @return O array da posição
     */
    public DVector getPos() {
        return pos;
    }

    /**
     * Atribui à posição do ponto
     * 
     * @param aPos O array da posição
     */
    public void setPos(DVector pos) {
        this.pos = pos;
    }
    
    //-------
    
    /**
     * Acesso à posição X do ponto
     * 
     * @return O valor da posição em X
     */
    public double getPosX() {
        return pos.getVecData(0);
    }

    /**
     * Acesso à posição Y do ponto
     * 
     * @return O valor da posição em Y
     */
    public double getPosY() {
        return pos.getVecData(1);
    }

    /**
     * Acesso à posição Z do ponto
     * 
     * @return O valor da posição em Z
     */
    public double getPosZ() {
        return pos.getVecData(2);
    }
    
    //-------
    
    /**
     * Atribui à posição X do ponto
     * 
     * @param x O valor da posição em X
     */
    public void setPosX(double x) {
        pos.setVecData(x,0);
    }

    /**
     * Atribui à posição Y do ponto
     * 
     * @param y O valor da posição em Y
     */
    public void setPosY(double y) {
        pos.setVecData(y,1);
    }

    /**
     * Atribui à posição Z do ponto
     * 
     * @param z O valor da posição em Z
     */
    public void setPosZ(double z) {
        pos.setVecData(z,2);
    }

    //-------
    
    /**
     * Acesso à normal do ponto
     * 
     * @return O array da normal
     */
    public DVector getNrm() {
        return nrm;
    }

    /**
     * Atribui à normal do ponto
     * 
     * @param aPos O array da normal
     */
    public void setNrm(DVector nrm) {
        this.nrm = nrm;
    }
    
    //-------
                
    /**
     * Acesso à componente X da normal
     * 
     * @return O valor da componente X da normal
     */
    public double getNrmX() {
        return nrm.getVecData(0);
    }

    /**
     * Acesso à componente Y da normal
     * 
     * @return O valor da componente Y da normal
     */
    public double getNrmY() {
        return nrm.getVecData(1);
    }
    
    /**
     * Acesso à componente Z da normal
     * 
     * @return O valor da componente Z da normal
     */
    public double getNrmZ() {
        return nrm.getVecData(2);
    }
    
    //-------
                
    /**
     * Atribui à componente X da normal
     * 
     * @param x O valor da componente X da normal
     */
    public void setNrmX(double x) {
        nrm.setVecData(x,0);
    }

    /**
     * Atribui à componente Y da normal
     * 
     * @param y O valor da componente Y da normal
     */
    public void setNrmY(double y) {
        nrm.setVecData(y,1);
    }
    
    /**
     * Atribui à componente Z da normal
     * 
     * @param z O valor da componente Z da normal
     */
    public void setNrmZ(double z) {
        nrm.setVecData(z,2);
    }
    
    //-------
    /**
     * Atribui a propriedade ao Objeto
     * 
     * @param Objeto 
     */
    public Object getObj() {
        return obj;
    }
    
    public void setObj(Object obj) {
        this.obj = obj;
    }

    public boolean equals(Point3D p){
        return p.getPosX() == this.getPosX() && p.getPosY() == this.getPosY() && p.getPosZ() == this.getPosZ();
    }
}
