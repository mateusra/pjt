/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * class SphCell
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.structures;

//java imports
import ic.apps.mateus.Particula;
import java.util.ArrayList;

public class GridCell {
    
    //posição do vértice base da célula
    private Point3D pos;
    //particulas dentro da celula
    private ArrayList<Particula> parts = new ArrayList<>();
    //particulas dentro da celula
    private ArrayList<Particula> neigs = new ArrayList<>();
    
    //construtor
    public GridCell(Point3D p){
        pos = p;
    }   

    //retorna a posição do vertice base da célula    
    public Point3D getPos() {
        return pos;
    }

    //atribui a posição do vertice base da célula    
    public void setPos(Point3D pos) {
        this.pos = pos;
    }
    
    //---
    
    //retorna o vetor de partículas    
    public ArrayList<Particula> getParts() {
        return parts;
    }

    //atribui o vetor de partículas    
    public void setParts(ArrayList<Particula> parts) {
        this.parts = parts;
    }
    
    //---
    
    //retorna o vetor de partículas    
    public ArrayList<Particula> getNeigs() {
        return neigs;
    }

    //atribui o vetor de partículas    
    public void setNeigs(ArrayList<Particula> neigs) {
        this.neigs = neigs;
    }
    
    //---
    
    //remove a partícula do vetor    
    public void dellPart( Particula part ){
        parts.remove(part);
    }
    
    //adiciona a partícula ao vetor    
    public void addPart( Particula part ){
        parts.add(part);                
    }
    
    //limpa o vetor de partículas    
    public void cleanParts(){
        parts.clear();
    }
    
    //---
        
    //remove a partícula do vetor    
    public void dellNeig( Particula neig ){
        neigs.remove(neig);
    }
    
    //adiciona a partícula ao vetor    
    public void addNeig( Particula neig ){
        neigs.add(neig);                
    }
    
    //limpa o vetor de partículas    
    public void cleanNeighs(){
        neigs.clear();
    }

    public boolean equals(Object c){
        return c instanceof GridCell && ((GridCell)c).getPos().equals(this.getPos());
    }
}
