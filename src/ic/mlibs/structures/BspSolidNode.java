/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * class PcNodeBSP
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */
package ic.mlibs.structures;

//java imports
import java.util.ArrayList;

public class BspSolidNode extends BspNode {

    //---Construtores
    
    public BspSolidNode(BspSolidNode node, int cls) {
        super(node,cls);
    }

    public BspSolidNode(BspSolidTree tr) {
        super(tr);
    }

    //---Implementações
    
    @Override
    //Divide o subespaço   
    public void split(BspPlane splane, ArrayList<Object> objs, ArrayList<Object> frontObjs, ArrayList<Object> backObjs) {
              
        //Armazena o hyperplano do nó corrente
        plane = splane;

        //pega o número de poligonos no nó atual; 
        int npolys = objs.size();

        //percorre os polígonos
        for (int i = 0; i < npolys; ++i) {
                        
            //Pega o poligono i
            BspPoly poly = (BspPoly)objs.get(i);
            
            //Poligonos na frente e atras do plano
            BspPoly polyFront = new BspPoly();
            BspPoly polyBack  = new BspPoly();
            
            //Testa o poligono em relação ao plano
            switch( poly.polyToPlane(plane, tree) ) {
                case BspConsts.CLASS_OVER:
                case BspConsts.CLASS_FRONT: {
                    frontObjs.add(poly);
                    break;
                }                   
                case BspConsts.CLASS_BACK: {
                    backObjs.add(poly); 
                    break;
                }                    
                case BspConsts.CLASS_CUT: {
                    poly.split(plane, polyFront, polyBack, tree);
                    
                    if(polyFront.getNpoints() >= 3 ) frontObjs.add(polyFront);
                    if(polyBack .getNpoints() >= 3 ) backObjs .add(polyBack );
                } 
            }
        }
        
        //cria os filhos
        front = new BspSolidNode(this, BspConsts.CLASS_FRONT);
        back  = new BspSolidNode(this, BspConsts.CLASS_BACK );
    }
}
