
/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Interface CheDops
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.structures;

public interface CheDops {
    //Cálculo da menor curvatura principal em vid
    double minCurvature(int vid);

    //Cálculo da maior curvatura principal em vid
    double maxCurvature(int vid);
    
    //Cálculo da curvatura media em vid
    double meanCurvature(int vid);
    
    //Cálculo da curvatura Gaussiana em vid
    double gaussCurvature(int vid);
    
    //Cálculo do laplaciano em vid
    double laplacian(int vid, double [] function);
}
