/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe Che
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.structures;

//File i-o imports
import ic.mlibs.util.Helper;
import java.io.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.*;

class Che {

    protected int nvert; //> Número de vértices da malha.
    protected int ntrig; //> Número de triangulos da malha.
    protected int nquad; //> Número de quadrangulos da malha.
    protected int ncomp; //> Número de componentes conexas da malha.
    protected int ncurv; //> Número de curvas de bordo da malha.
    protected ArrayList<Point3D> G;  //> Coordenada dos vértices da malha.
    protected ArrayList<Integer> VT; //> Índice dos vértices dos trigs da malha.
    protected ArrayList<Integer> VQ; //> Índice dos vértices dos quads da malha.
    protected ArrayList<Integer> OT; //> Conteiner de half-edges vizinhas de trigs.
    protected ArrayList<Integer> OQ; //> Container de half-edges vizinhas de quads.
    protected HashMap
                <Integer,Object> EH; //> Representação de arestas.
    protected ArrayList<Integer> VH; //> Primeiro triângulo da estrela de um vértice.
    protected ArrayList<Integer> C;  //> Componente conexa de cada vértice.    
    protected ArrayList<Integer> B;  //> Half-edge representante de cada curva de bordo.    

    //------
    public Che() {
    }

    public Che(String file) {
        readChe(file);
    }

    //------
    /**
     * Acesso ao número de vértices da malha.
     */
    public int getNvert() {
        return nvert;
    }

    /**
     * Acesso ao número de arestas da malha.
     */
    public int getNedge() {
        return EH.size();
    }

    /**
     * Acesso ao número de half-edges da malha.
     */
    public int getNhe() {
        return 3 * ntrig + 4 * nquad;
    }

    /**
     * Acesso ao número de trigs da malha.
     */
    public int getNtrig() {
        return ntrig;
    }

    /**
     * Acesso ao número de quads da malha.
     */
    public int getNquad() {
        return nquad;
    }

    /**
     * Acesso ao número de faces da malha.
     */
    public int getNface() {
        return ntrig + nquad;
    }

    /**
     * Acesso ao número de componentes conexas.
     */
    public int getNcomp() {
        return ncomp;
    }

    /**
     * Acesso ao número de curvas de bordo.
     */
    public int getNcurv() {
        return ncurv;
    }

    //------
    /** 
     * Testa se a face é um trig ou um quad.
     */
    boolean isTrig(int f) {
        return (f < getNtrig() && f >= 0);
    }

    /** 
     * Testa se a half edge pertence a um trig ou um quad. 
     */
    boolean isTrigHe(int h) {
        return (h < 3 * getNtrig() && h >= 0);
    }

    //------
    /**
     * Acesso ao vertice v da malha.
     */
    public Point3D getG(int v) {
        return G.get(v);
    }

    /**
     * Acesso ao vértice da half edge h da malha.
     */
    public int getV(int h) {
        return (isTrigHe(h)) ? (VT.get(h)) : (VQ.get(h - 3 * getNtrig()));
    }

    /**
     * Acesso à oposta da half edge h da malha.
     */
    public int getO(int h) {
        return (isTrigHe(h)) ? (OT.get(h)) : (OQ.get(h - 3 * getNtrig()));
    }

    /**
     * Acesso à half edge do vertice v da malha.
     */
    public int getVH(int v) {
        return VH.get(v);
    }

    /**
     * Acesso à half edge do vertice v da malha.
     */
    public Object getEH(int e) {
        int he = (getO(e) >= 0) ? (Math.min(e, getO(e))) : (e);
        return EH.get(he);
    }

    /**
     * Acesso à componente do vertice v da malha.
     */
    public int getC(int v) {
        return C.get(v);
    }

    /**
     * Acesso à halfedge da curva de bordo b da malha.
     */
    public int getB(int b) {
        return B.get(b);
    }

    //------
    /**
     * Atribui o valor do vertice v da malha.
     */
    public void setG(Point3D p, int v) {
        G.set(v, p);
    }

    /**
     * Atribui o valor do vértice da half edge h da malha.
     */
    public void setV(int v, int h) {
        if (isTrigHe(h)) {
            VT.set(h, v);
        } else {
            VQ.set(h - 3 * getNtrig(), v);
        }
    }

    /**
     * Atribui o valor da oposta da half edge h da malha.
     */
    public void setO(int o, int h) {
        if (isTrigHe(h)) {
            OT.set(h, o);
        } else {
            OQ.set(h - 3 * getNtrig(), o);
        }
    }

    /**
     * Atribui o valor da half edge do vertice v da malha.
     */
    public void setVH(int h, int v) {
        VH.set(v, h);
    }

    /**
     * Atribui a aresta relacionada a half-edge e da malha.
     */
    public void setEH(int e) {
        int he = (getO(e) >= 0) ? (Math.min(e, getO(e))) : (e);
        EH.put(he, getO(he));
    }
    /**
     * Atribui a aresta relacionada a aresta e da malha.
     */
    public void setEH(int eid, Object p) {
        EH.put(eid, p);
    }

    /**
     * Atribui o valor da componente do vertice v da malha.
     */
    public void setC(int c, int v) {
        C.set(v, c);
    }

    /**
     * Atribui o valor da halfedge da curva de bordo b da malha.
     */
    public void getB(int h, int b) {
        B.set(b, h);
    }

    //------
    /** 
     * Acesso ao trig da half-edge h da malha.   
     */
    public int trig(int h) {
        return h / 3;
    }

    /** 
     * Acesso ao quad da half-edge h da malha. 
     */
    public int quad(int h) {
        return (h - 3 * getNtrig()) / 4;
    }

    /** 
     * Acesso à face da half-edge h da malha.       
     */
    public int face(int h) {
        return (isTrigHe(h)) ? (trig(h)) : (quad(h) + getNtrig());
    }

    /** 
     * Acesso à half-edge "next" de uma half-edge h.     
     */
    public int next(int h) {
        return (isTrigHe(h)) ? (3 * trig(h) + (h + 1) % 3) : ((4 * quad(h) + 3 * getNtrig()) + ((h - 3 * getNtrig()) + 1) % 4);
    }

    /** 
     * Acesso à half-edge "midd" de uma half-edge h.     
     */
    public int midd(int h) {
        return (isTrigHe(h)) ? (-1) : ((4 * quad(h) + 3 * getNtrig()) + ((h - 3 * getNtrig()) + 2) % 4);
    }

    /** 
     * Acesso à half-edge "prev" de uma half-edge h. 
     */
    public int prev(int h) {
        return (isTrigHe(h)) ? (3 * trig(h) + (h + 2) % 3) : ((4 * quad(h) + 3 * getNtrig()) + ((h - 3 * getNtrig()) + 3) % 4);
    }

    /** 
     * Acesso à primeira half-edge da face de uma half-edge h. 
     */
    public int base(int f) {
        return (isTrig(f)) ? (3 * f) : (4 * (f - getNtrig()) + 3 * getNtrig());
    }

    /**  
     * Iteração sobre a face da half-edge h.
     */
    public int fit(int h, int n) {
        int it = h;
        for (int j = 0; j < n; ++j) {
            it = next(it);
        }
        return it;
    }

    /**  
     * Acesso à half-edge do vértice v na face f.          
     */
    public int vhe(int v, int f) {
        int n = isTrig(f) ? 3 : 4;
        int h = base(f);

        for (int i = h; i < (h + n); ++i) {
            if (getV(i) == v) {
                return i;
            }
        }
        return -1;
    }

    //------
    /** 
     * Testa se um vertice está no bordo. 
     */
    public boolean vIsOnBound(int v) {
        return (getO(getVH(v)) == -1);
    }

    /** 
     * Testa se uma aresta está no bordo.  
     */
    public boolean eIsOnBound(int e) {
        return (getO(e) == -1);
    }

    /** 
     * Testa se uma face está no bordo.  
     */
    public boolean fIsOnBound(int f) {
        int h = base(f);
        return (getO(fit(h, 0)) == -1 || getO(fit(h, 1)) == -1 || getO(fit(h, 2)) == -1 || getO(fit(h, 3)) == -1);
    }

    /** 
     * Acesso à próxima half-edge no bordo.   
     */
    public int nextOnBound(int h) {
        return getVH(getV(next(h)));
    }

    /** 
     * Tests if a boundary is valid.   
     */
    public int prevOnBound(int h) {
        ArrayList<Integer> r00 = R_01(getV(h));
        int size = r00.size();

        return r00.get(size - 1);
    }

    //-----
    /** 
     * Calcula os vertices da estrela de um vértice.  
     */
    public ArrayList<Integer> R_00(int v) {
        ArrayList<Integer> r00 = new ArrayList<Integer>();

        //Gets the first incident half--edge
        int h = getVH(v), //> current half-edge
                hl = h, //> last half-edge
                h0 = h;        //> first half-edge

        // Walks through the single wise.  
        do {
            hl = h;
            //Pushes the first vertex
            r00.add(getV(next(hl)));
            //Pushes the second vertex, iff is a quad
            if (!isTrigHe(hl)) {
                r00.add(getV(midd(hl)));
            }
            //Goes to the next face
            h = getO(prev(hl));
        } while ((h != -1) && (h != h0));

        //The last vertex of the single wise...
        if (h == -1) {
            r00.add(getV(prev(hl)));
        }

        return r00;
    }

    /** 
     * Calcula as half-edges na estrela de um vérice. 
     */
    public ArrayList<Integer> R_01(int v) {
        ArrayList<Integer> r01 = new ArrayList<Integer>();

        //Gets the first incident half--edge
        int h = getVH(v), //> current half-edge
                hl = h, //> last half-edge
                h0 = h;        //> first half-edge

        //Walks through the single wise.  
        do {
            hl = h;
            // Pushes the face
            r01.add(hl);
            // Jumps to the next face 
            h = getO(prev(hl));
        } while ((h != -1) && (h != h0));

        return r01;
    }

    /** 
     * Calcula as faces na estrela de um vértice. 
     */
    public ArrayList<Integer> R_02(int v) {
        ArrayList<Integer> r02 = new ArrayList<Integer>();

        //Gets the first incident half--edge
        int h = getVH(v), //> current half-edge
                hl = h, //> last half-edge
                h0 = h;        //> first half-edge

        //Walks through the single wise.  
        do {
            hl = h;
            // Pushes the face
            r02.add(face(hl));
            // Jumps to the next face 
            h = getO(prev(hl));
        } while ((h != -1) && (h != h0));

        return r02;
    }

    /** 
     * Calcula os vertices na estrela de uma aresta.    
     */
    public ArrayList<Integer> R_10(int h) {
        ArrayList<Integer> r10 = new ArrayList<Integer>();

        //Pushes the vertices of the current face
        r10.add(getV(prev(h)));
        if (!isTrigHe(h)) //> only for quads
        {
            r10.add(getV(midd(h)));
        }

        //Pushes the vertices of the neighbor face, if any.
        if (getO(h) != -1) {
            int o = getO(h);
            //Push the other face's vertices
            r10.add(getV(prev(o)));
            if (!isTrigHe(o)) //> only for quads
            {
                r10.add(getV(midd(o)));
            }
        }

        return r10;
    }

    /** 
     * Calcula as faces da estrela de uma aresta.   
     */
    public ArrayList<Integer> R_12(int h) {
        ArrayList<Integer> r12 = new ArrayList<Integer>();

        //Pushes the first face
        r12.add(face(h));

        //Pushes the neighbor face, if any.
        if (getO(h) != -1) {
            r12.add(face(getO(h)));
        }

        return r12;
    }

    /** 
     * Calcula as faces adjacentes a uma face.  
     */
    public ArrayList<Integer> R_22(int f) {
        ArrayList<Integer> r22 = new ArrayList<Integer>();

        //Gets the base half-edge
        int h = base(f);

        //Gets the opposite faces
        if (getO(h) != -1) {
            r22.add(face(getO(h)));
        }
        if (getO(h + 1) != -1) {
            r22.add(face(getO(h + 1)));
        }
        if (getO(h + 2) != -1) {
            r22.add(face(getO(h + 2)));
        }

        //Gets the last face, if a quad
        if (!isTrig(f) && getO(h + 3) != -1) {
            r22.add(face(getO(h + 3)));
        }

        return r22;
    }

    //------
    
    public void readChe(String file) {
        try {
            Scanner scan;

            FileReader read = new FileReader(file);
            BufferedReader buf = new BufferedReader(read);

            //Testa se eé um OFF
            String magic = buf.readLine();
            if (!magic.equalsIgnoreCase("OFF")) 
                return;

            scan = new Scanner(buf.readLine());

            //Leitura do número de vertices
            nvert = Integer.parseInt(scan.next());
            ntrig = nquad = 0;
            //Lixo
            int tmp0 = Integer.parseInt(scan.next());
            int tmp1 = Integer.parseInt(scan.next());
            
            ArrayList<Point3D> tempG = new ArrayList<Point3D>(nvert);
            for (int i = 0; i < nvert; i++) {
                scan = new Scanner(buf.readLine());

                Point3D p = new Point3D();

                double x = Double.parseDouble(scan.next());
                double y = Double.parseDouble(scan.next());
                double z = Double.parseDouble(scan.next());

                p.setPosX(x);
                p.setPosY(y);
                p.setPosZ(z);
                p.setObj(new Double(2));

                tempG.add(p);
            }

            ArrayList<Integer> tempVT = new ArrayList<Integer>();
            ArrayList<Integer> tempVQ = new ArrayList<Integer>();

            String line;
            while ( (line = buf.readLine()) != null ) {
                scan = new Scanner(line);

                int tORq = Integer.parseInt(scan.next());

                if(tORq == 3) {
                    tempVT.add(Integer.parseInt(scan.next()));
                    tempVT.add(Integer.parseInt(scan.next()));
                    tempVT.add(Integer.parseInt(scan.next()));
                    ntrig++;
                } 
                else if(tORq == 4) {
                    tempVQ.add(Integer.parseInt(scan.next()));
                    tempVQ.add(Integer.parseInt(scan.next()));
                    tempVQ.add(Integer.parseInt(scan.next()));
                    tempVQ.add(Integer.parseInt(scan.next()));
                    nquad++;
                }
            }

            buf.close();

            //Aloca a Che            
            allocChe();
            
            //Copia o dado lido
            Collections.copy( G,tempG );
            Collections.copy(VT,tempVT);
            Collections.copy(VQ,tempVQ);

            // Cria a estrutura Che da malha lida
            build();


        } catch (IOException ex) {
            System.out.println("Erro em Che::readChe.");
            ex.printStackTrace();
        }
    }

    public void writeChe(String file) {
        try {
            FileOutputStream fos = new FileOutputStream(new File(file));
            DataOutputStream dat = new DataOutputStream(fos);

            dat.writeChars("OFF");

            dat.writeInt(nvert);
            dat.writeInt(ntrig);
            dat.writeInt(nquad);

            for (int i = 0; i < nvert; i++) {
                Point3D p = new Point3D();

                dat.writeDouble(p.getPosX());
                dat.writeDouble(p.getPosY());
                dat.writeDouble(p.getPosZ());
            }

            for (int i = 0; i < ntrig; i++) {
                dat.writeInt(3);

                dat.writeInt(VT.get(3 * i));
                dat.writeInt(VT.get(3 * i + 1));
                dat.writeInt(VT.get(3 * i + 2));
            }

            for (int i = 0; i < nquad; i++) {
                dat.writeInt(3);

                dat.writeInt(VQ.get(4 * i));
                dat.writeInt(VQ.get(4 * i + 1));
                dat.writeInt(VQ.get(4 * i + 2));
                dat.writeInt(VQ.get(4 * i + 3));
            }

            dat.flush();
            dat.close();

        } catch (IOException ex) {
            System.out.println("Erro em Che::writeChe.");
            ex.printStackTrace();
        }
    }

    //------
    
    public void computeNormals() {
        double[] nrm = new double[3];
        System.out.print("Che::computeNormals -> ");

        //Erases the normals
        for (int i = 0; i < nvert; i++) {
            Point3D p = getG(i);
            p.setNrmX(0);
            p.setNrmY(0);
            p.setNrmZ(0);
        }

        //Run on the faces
        for (int i = 0; i < getNface(); i++) {

            //Gets the base half--edge
            int h = base(i);

            Point3D v0 = getG(getV(h));
            Point3D v1 = getG(getV(h + 1));
            Point3D v2 = getG(getV(h + 2));

            //Computes the normal
            Helper.normal(v0, v1, v2, nrm);

            //Updates the normals
            v0.setNrmX(v0.getNrmX() + nrm[0]);
            v1.setNrmX(v1.getNrmX() + nrm[0]);
            v2.setNrmX(v2.getNrmX() + nrm[0]);

            v0.setNrmY(v0.getNrmY() + nrm[1]);
            v1.setNrmY(v1.getNrmY() + nrm[1]);
            v2.setNrmY(v2.getNrmY() + nrm[1]);

            v0.setNrmZ(v0.getNrmZ() + nrm[2]);
            v1.setNrmZ(v1.getNrmZ() + nrm[2]);
            v2.setNrmZ(v2.getNrmZ() + nrm[2]);

            //Only for quads
            if (!isTrig(i)) {
                Point3D v3 = getG(getV(h + 3));

                v3.setNrmX(v3.getNrmX() + nrm[0]);
                v3.setNrmY(v3.getNrmY() + nrm[1]);
                v3.setNrmZ(v3.getNrmZ() + nrm[2]);
            }
        }

        // Normalização das normais
        for (int i = 0; i < nvert; i++) {
            Point3D v = getG(i);

            double mod = (double) Math.hypot(v.getPosX(), Math.hypot(v.getPosY(), v.getPosZ()));
            if (mod < Double.MIN_VALUE) {
                mod = Double.MIN_VALUE;
            }

            v.setNrmX(v.getNrmX() / mod);
            v.setNrmY(v.getNrmY() / mod);
            v.setNrmZ(v.getNrmZ() / mod);
        }

        System.out.println("Sucesso !");
    }

    public void flipNormals() {

        System.out.print("Che::flipNormals -> ");

        //Run on the vertices
        for (int i = 0; i < nvert; i++) {
            Point3D p = getG(i);

            p.setNrmX(-1 * p.getNrmX());
            p.setNrmY(-1 * p.getNrmY());
            p.setNrmZ(-1 * p.getNrmZ());
        }

        System.out.println("Sucesso !");
    }

    public void getBbox(double[] min, double[] max) {

        double t_mx, t_Mx, t_my, t_My, t_mz, t_Mz;

        //Starts the limiters
        t_mx = t_Mx = G.get(0).getPosX();
        t_my = t_My = G.get(0).getPosY();
        t_mz = t_Mz = G.get(0).getPosZ();

        //Run on the vertices
        for (int i = 1; i < getNvert(); ++i) {
            if (G.get(i).getPosX() < t_mx) {
                t_mx = G.get(i).getPosX();
            }
            if (G.get(i).getPosX() > t_Mx) {
                t_Mx = G.get(i).getPosX();
            }

            if (G.get(i).getPosY() < t_my) {
                t_my = G.get(i).getPosY();
            }
            if (G.get(i).getPosY() > t_My) {
                t_My = G.get(i).getPosY();
            }

            if (G.get(i).getPosZ() < t_mz) {
                t_mz = G.get(i).getPosZ();
            }
            if (G.get(i).getPosZ() > t_Mz) {
                t_Mz = G.get(i).getPosZ();
            }
        }

        //Sets the bounding box
        min[0] = t_mx;
        min[1] = t_my;
        min[2] = t_mz;
        max[0] = t_Mx;
        max[1] = t_My;
        max[2] = t_Mz;
    }

    public void rescale() {

        System.out.print("Che::rescale -> ");

        double[] c = new double[3];
        double[] l = new double[3];

        double[] min = new double[3];
        double[] max = new double[3];

        //Calcula a caixa envolvente
        getBbox(min, max);

        //Gets the center of the bounding box
        c[0] = (double) (max[0] + min[0]) / 2;
        c[1] = (double) (max[1] + min[1]) / 2;
        c[2] = (double) (max[2] + min[2]) / 2;

        //Computes the box sides
        double size = 0;
        for (int i = 0; i < 3; i++) {
            l[i] = Math.abs(max[i] - min[i]);
            if (l[i] > size) {
                size = l[i];
            }
        }

        //Updates the vertices geometry
        for (int i = 0; i < getNvert(); i++) {
            double tx = G.get(i).getPosX();
            double ty = G.get(i).getPosY();
            double tz = G.get(i).getPosZ();

            tx -= c[0];
            if (size != 0) {
                tx /= .5f * size;
            }
            G.get(i).setPosX(tx);

            ty -= c[1];
            if (size != 0) {
                ty /= .5f * size;
            }
            G.get(i).setPosY(ty);

            tz -= c[2];
            if (size != 0) {
                tz /= .5f * size;
            }
            G.get(i).setPosZ(tz);
        }

        System.out.println("Sucesso !");
        System.out.println();
    }

    //------
    
    protected void allocChe() {
        if (nvert == 0 || (ntrig == 0 && nquad == 0)) {
            return;
        }

        //São criados sequencialmentes
        G  = new ArrayList<Point3D>(nvert);
        VH = new ArrayList<Integer>(nvert);
        for (int i = 0; i < nvert; i++) {
             G.add(null);
            VH.add( -1 );
        }

        //Criados randomicamente
        VT = new ArrayList<Integer>(3 * ntrig);
        OT = new ArrayList<Integer>(3 * ntrig);
        for (int i = 0; i < 3 * ntrig; i++) {
            VT.add( -1 );
            OT.add( -1 );
        }

        VQ = new ArrayList<Integer>(4 * nquad);
        OQ = new ArrayList<Integer>(4 * nquad);
        for (int i = 0; i < 4 * nquad; i++) {
            VQ.add( -1 );
            OQ.add( -1 );
        }

        //Tamanho desconhecido a priori
        EH = new HashMap<Integer, Object>();
        C = new ArrayList<Integer>();
        B = new ArrayList<Integer>();
    }

    protected void build() {
        //opostos
        computeO();
        //orientaçao
        orient();

        //células explícitas
        computeVH();
        computeEH();

        //componentes and curvas
        computeC();
        computeB();

        //cálculo das normais
        computeNormals();

        //normaliza o modelo
        rescale(); 
    }

    private void computeO() {
        //Evita erros quando os containeres estão vazios
        if (VQ.isEmpty() && VT.isEmpty()) {
            return;
        }

        System.out.print("Che::computeO -> ");

        HashMap<SimpleEntry<Integer,Integer>,Integer> adjacency = 
                new HashMap<SimpleEntry<Integer,Integer>,Integer>();
        adjacency.clear();

        //Percorre as half-edges
        for (int c = 0; c < getNhe(); ++c) {
            //Gets the vertices of the half-edges
            int a = getV(c);
            int b = getV(next(c));
            if (b < a) {
                int tmp = a;
                a = b;
                b = tmp;
            }

            SimpleEntry<Integer,Integer> se = new SimpleEntry<Integer,Integer>(a, b);
            Integer v = (Integer) adjacency.get(se);

            //If found, match the opposites and erase on the temp map
            if (v != null) {
                //Matches
                setO(c, v);
                setO(v, c);
                adjacency.remove(se);
            } //If dont, create on the temp map
            else //Inclusion
            {
                adjacency.put(se, c);
            }
        }
        adjacency.clear();
        System.out.println("Sucesso !");
    }

    private void computeVH() {
        //Avoid errors when the containers are empty
        if (VH.isEmpty() || (OT.isEmpty() && OQ.isEmpty())) {
            return;
        }

        System.out.print("Che::computeVH -> ");
        
        //Run on the half-edges
        for (int i = 0; i < getNhe(); ++i) {
            //Sets if it is a boundary half-edge
            if (getO(i) == -1) {
                setVH(i, getV(i));
            } else //Changes only it was not discovered yet
            if (getVH(getV(i)) == -1) {
                setVH(i, getV(i));
            }
        }
        System.out.println("Sucesso !");
    }

    private void computeEH() {
        //Avoid errors when the containers are empty
        if (G.isEmpty() || (VT.isEmpty() && VQ.isEmpty())) {
            return;
        }

        System.out.print("Che::computeEH -> ");

        //Run on the half-edges
        for (int i = 0; i < getNhe(); ++i) {

            //Gets the edge
            int he0 = i;
            int he1 = getO(i);

            //If it is a boundary edge
            if (he1 == -1) {
                EH.put(he0, he1);
            } else {
                
                int min = Math.min(he0, he1);
                int max = Math.max(he0, he1);

                if (!EH.containsKey(min)) {
                    EH.put(min, max);
                }
            }
        }
        System.out.println(EH.size() + " arestas encontradas.");
    }

    private void computeC() {
        //Avoid errors when the containers are empty
        if ((VQ.isEmpty() && VT.isEmpty()) ) {
            return;
        }

        System.out.print("Che::computeC -> ");

        //Initialization
        ncomp = 0;

        //Each vertex is a component
        for (int v = 0; v < nvert; ++v) {
            C.add(v);
        }

        //Run on the faces
        for (int j = 0; j < getNface(); ++j) {

            int he = base(j);

            //Gets the current component
            int b0 = getComponent(getV(  he  ));
            int b1 = getComponent(getV(he + 1));
            int b2 = getComponent(getV(he + 2));

            //Sets the component as the minimum value
            if (isTrig(j)) {
                trigComponent(b0, b1, b2);
            } else { //> Quads
                int b3 = getComponent(getV(he + 3));
                quadComponent(b0, b1, b2, b3);
            }
        }

        //Components renumbering
        HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
        for (int v = 0; v < getNvert(); ++v) {

            //Gets the vertex component
            int b = getComponent(v);
            if (b < 0) {
                continue;
            }

            //Old vs new numbering
            if (!m.containsKey(b)) {
                m.put(b, ncomp++);
            }
        }

        //Sets the components
        for (int v = 0; v < nvert; ++v) {

            //Gets the component
            int b = getC(v);
            if (b < 0) {
                continue;
            }

            //Sets the component
            setC(m.get(b),v);
        }
        m.clear();

        System.out.println(ncomp + " componentes conexas encontradas.");

    }

    private void computeB() {
        //Avoid errors when the containers are empty
        if (OT.isEmpty() && OQ.isEmpty()) {
            return;
        }

        System.out.print("Che::computeB -> ");

        //Resize the visited container
        boolean [] vst = new boolean[getNhe()];
        
        ncurv = 0;

        //Run on the half-edges
        for (int he = 0; he < getNhe(); ++he) {
            //Gets the first unvisited half-edge
            if (getO(he) == -1 && vst[he] == Boolean.FALSE) {
                int he0 = he;

                //Update the number of curves
                ncurv++;
                B.add(he0);

                //Run through the curve
                do {
                    vst[he0] = Boolean.TRUE;
                    while (getO(next(he0)) != -1) {
                        he0 = getO(next(he0));
                    }
                    he0 = next(he0);
                } while (he0 != he);
            }
        }

        System.out.println(ncurv + " curvas de bordo encontradas.");
    }

    private void orient() {
        if ((VT.isEmpty() && VQ.isEmpty()) || G.isEmpty()) {
            return;
        }

        System.out.print("Che::orient -> ");

        //Pilha de faces que podem ser visitadas
        Stack<Integer> s = new Stack<Integer>();

        //Lista com o estado atual das visitas
        ArrayList<Boolean> visited = new ArrayList<Boolean>(getNface());
        for (int i = 0; i < getNface(); i++) {
            visited.add(Boolean.FALSE);
        }

        for (int i = 0; i < getNface(); ++i) {

            if (visited.get(i)) {
                continue;
            }

            /** New connected compounds found*/
            int hc = base(i);

            s.push(hc);
            s.push(hc + 1);
            s.push(hc + 2);
            if (!isTrig(i)) {
                s.push(hc + 3);
            }

            visited.set(i, true);

            while (!s.empty()) {
                int h = s.peek();
                s.pop();

                /** Avoid null edges*/
                int o = getO(h);
                if (o == -1) {
                    continue;
                }

                /** Avoid Loops*/
                int t = face(o);
                if (visited.get(t)) {
                    continue;
                }

                /** Repairs orientation*/
                if (!orientCheck(h, o)) {
                    orientChange(t);
                }

                /** Marks as visited*/
                visited.set(t, true);

                /** Push half-edges of t*/
                int f = base(t);

                s.push(f);
                s.push(f + 1);
                s.push(f + 2);
                if (!isTrig(t)) {
                    s.push(f + 3);
                }
            }
        }

        System.out.println("Sucesso !");
    }

    // ----
    private boolean orientCheck(int h, int o) {
        if (o == -1) {
            return true;
        }

        int v1 = getV(h);
        int v2 = getV(next(h));

        int v3 = getV(o);
        int v4 = getV(next(o));

        if (!(v1 == v4 && v2 == v3) && !(v1 == v3 && v2 == v4)) {
            System.err.print("aresta inválida !");
        }

        return (v1 == v4 && v2 == v3);
    }

    private boolean orientChange(int f) {
        int h0 = base(f);
        int h1 = h0 + 1;
        int h2 = h0 + 2;
        int h3 = h0 + 3;

        if (isTrig(f)) {
            /**Exchange trig vertices */
            setV(h0, getV(h1));
            setV(h1, getV(h0));

            /**Exchange trig mates*/
            int o1 = getO(h1);
            int o2 = getO(h2);
            setO(h1, o2);
            setO(h2, o1);
            if (o1 != -1) {
                setO(o1, h2);
            }
            if (o2 != -1) {
                setO(o2, h1);
            }
        } else {
            /**Exchange quad vertices */
            setV(h0, getV(h1));
            setV(h1, getV(h0));

            setV(h2, getV(h3));
            setV(h3, getV(h2));

            /**Exchange quad mates*/
            int o1 = getO(h1);
            int o3 = getO(h3);
            setO(h1, o3);
            setO(h3, o1);
            if (o1 != -1) {
                setO(o1, h3);
            }
            if (o3 != -1) {
                setO(o3, h1);
            }
        }

        return false;
    }

    private int getComponent(int i) {
        int b = getC(i);
        
        if(i != b){
            //copia o vértice
            int v = i;
            while(v != b){
                //atualiza o vértics
                v = b;
                //atualiza a compontente
                b = getC(v);
            }
            //componente descoberta
            setC(b, i);
        }
        
        return b;
    }

    private void trigComponent(int b0, int b1, int b2) {
        int min = Math.min(b0, Math.min(b1, b2));

        if (b0 == min) {
            setC(min, b1);
            setC(min, b2);
        }
        else if (b1 == min) {
            setC(min, b0);
            setC(min, b2);
        }
        else if (b2 == min) {
            setC(min, b0);
            setC(min, b1);
        }
    }

    private void quadComponent(int b0, int b1, int b2, int b3) {
        int min = Math.min(Math.min(b0, b1), Math.min(b2, b3));

        if (b0 == min) {
            setC(min, b1);
            setC(min, b2);
            setC(min, b3);
        }
        else if (b1 == min) {
            setC(min, b0);
            setC(min, b2);
            setC(min, b3);
        }
        else if (b2 == min) {
            setC(min, b0);
            setC(min, b1);
            setC(min, b3);
        }
        else if (b3 == min) {
            setC(min, b0);
            setC(min, b1);
            setC(min, b2);
        }
    }
}
