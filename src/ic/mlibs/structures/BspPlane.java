/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Interface PlaneBSP
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */
package ic.mlibs.structures;

//meus imports
import ic.mlibs.util.Helper;

public class BspPlane {

    private Point3D n; //normal ao plano
    private double  d;   //nx*x+ny*y+nz*z = d : Equação do plano

    //Construtor
    BspPlane(Point3D nrm, double scalar) {
        n = nrm;
        d = scalar;
    }
    
    //--- metodos
    
    //distancia com sinal ponto -- plano
    public double signDistance(Point3D p) {
        return Helper.dot(p, n) - d;
    }

    //interseção plano - aresta
    public Point3D intersectEdge(Point3D vA, Point3D vB) {
        // convex coordiate of the intersection
        double t = (d - Helper.dot(vA, n)) / Helper.dot( Helper.sub(vB, vA), n);

        //ocorreu a interseção
        if( t>=-BspConsts.ERROR_TRASHOLD & t<=1+BspConsts.ERROR_TRASHOLD ){        
        
            double [] pos = { vA.getPosX() + t * (vB.getPosX() - vA.getPosX()), 
                              vA.getPosY() + t * (vB.getPosY() - vA.getPosY()), 
                              vA.getPosZ() + t * (vB.getPosZ() - vA.getPosZ()) } ;

            // coordinates of the intersection
            return new Point3D( pos );
        }
        else
            return null;
    }

    //testa se o plano é válido
    public boolean isValid() {
        return (   !Double.isInfinite(n.getPosX()) && !Double.isNaN(n.getPosX())
                && !Double.isInfinite(n.getPosY()) && !Double.isNaN(n.getPosY())
                && !Double.isInfinite(n.getPosZ()) && !Double.isNaN(n.getPosZ())
                && !Double.isInfinite(d) && !Double.isNaN(d));
    }

    //testa a posição de um ponto em relação ao plano
    public boolean front(Point3D p) {
        return signDistance(p) > BspConsts.ERROR_TRASHOLD;
    }
    
    //testa a posição de um ponto em relação ao plano
    public boolean back(Point3D p) {
        return signDistance(p) <-BspConsts.ERROR_TRASHOLD;
    }
    
    //testa a posição de um ponto em relação ao plano
    public int pointToPlane(Point3D p) {
        if     ( front(p) ) return BspConsts.CLASS_FRONT; //> está na frente
        else if( back (p) ) return BspConsts.CLASS_BACK;  //> está atras
        else return BspConsts.CLASS_OVER;                 //> é coplanar
    }      
}
