/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * class TreeBSP
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */
package ic.mlibs.structures;
       
//imports do Java
import java.util.Stack;
import java.util.ArrayList;

public abstract class BspTree {
    
    //--- Geometria
    protected ArrayList<Point3D> G; //vértices do espaço
    protected ArrayList< Object> P; //Lista de objetos a serem divididos
    
    //--- Nó raiz da BSP
    protected BspNode root;   
    
    public BspTree ( ){       
        //aloca a geometria
        G = new ArrayList<Point3D>();
        P = new ArrayList<Object >();
    }

    //--- Acessores e atribuidores
   
    //retorna o ponto id
    public Point3D getG(int id){
        return G.get(id);
    }

    //insere um vértice na geometria
    public int addG(Point3D v) {
        G.add(v);
        return (int) G.size() - 1;
    }
   
    //retorna o poligono i da lista
    public Object getP(int id){
        return P.get(id);
    }

    //insere um poligono na lista
    public int addP(Object p) {
        P.add(p);
        return (int) P.size() - 1;
    }

    //insere um poligono na lista
    public BspNode getRoot() {
        return root;
    }
    
    //--- Metodos
    
    //constroi a BSP
    public void buildBSPTree() {

        System.out.print("TreeBSP::buildBSPTree --> ");

        //Pilha de nós que serão percorridos
        Stack<BspNode> nodeStack = new Stack<BspNode>();
        nodeStack.push(root);
        
        //Pilha de poligonos a serem processados
        Stack<ArrayList<Object>> objStack  = new Stack<ArrayList<Object>>();
        objStack.push(P);
                
        //conta o numero de nós criados
        int counter=0;
        
        while ( nodeStack.size() > 0 ) {
                        
            //incrementa o contador
            counter++;
            
            //pega o próximo nó da pilha
            BspNode  node  = nodeStack.pop();
            //Lista de Poligonos
            ArrayList<Object> nodeObj = objStack.pop();
            
            //retorna se o nó não contém poligonos
            if( nodeObj.isEmpty() ) {
                continue;                
            }
            
            //retorna se o nó é nulo
            if( node == null ){
                System.out.println("TreeBSP::bluidBSPTree -- node == null");
                continue;
            }
            
            //usa o melhor plano para a
            BspPlane plane = chooseBestPlane(nodeObj);
            
            //Testa se calculou o plano corretamente
            if( !plane.isValid() ) {
                System.out.println("TreeBSP::bluidBSPTree -- plano invalido");
                continue; 
            }
            
            //Lista de Poligonos dos filhos
            ArrayList<Object> frontObj = new ArrayList<Object>();
            ArrayList<Object>  backObj = new ArrayList<Object>();
            //divide os poligonos entre os filhos
            node.split(plane, nodeObj, frontObj, backObj); 
            
            //Inclui os nos na pilha
            nodeStack.add(node.getFront());
            nodeStack.add(node.getBack() );         
            
            //Inclui os poligonos na pilha
            objStack.add(frontObj);
            objStack.add( backObj);
        }
        
        System.out.println( counter + " nodes processed.");
    }
    
    //--- métodos privados
    
    //constroi a geometria a partir de uma GLMesh
    protected abstract void buildGeometry( Object o );
    
    //escolhe um bom plano para subdividir a bsp
    protected abstract BspPlane chooseBestPlane(ArrayList<Object> objs);
}
