/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * class ConstsBSP
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */
package ic.mlibs.structures;

public class BspConsts {
    //Classidicador em relação aos planos
    public final static int   CLASS_INVALID   =-1;
    public final static int   CLASS_FRONT     = 0;
    public final static int   CLASS_BACK      = 1;
    public final static int   CLASS_OVER      = 2;
    public final static int   CLASS_CUT       = 3;    
    //Tolerancia para erros numericos
    public final static double ERROR_TRASHOLD = 0.000000001;
}
