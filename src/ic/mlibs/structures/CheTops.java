
/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Interface CheTops
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.structures;

interface CheTops {
    //Operação de Colapso de arestas
    void EdgeColapse(int h);
    
    //Operação de Subdivisão de arestas
    void EdgeSplit(int h);
}
