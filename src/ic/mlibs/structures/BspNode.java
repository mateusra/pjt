/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * class NodeBSP
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */
package ic.mlibs.structures;

import java.util.ArrayList;

public abstract class BspNode {

    protected BspTree  tree ; //referência da arvore a que pertence o nó
    protected BspNode  front; //no da frente - exterior
    protected BspNode  back ; //no de trás - interior
    protected BspPlane plane; //hiperplano que subdivide o nó
    
    protected int      level; //nivel 
    protected int      type;  //o nó é da frente ou de trás ?
    
    //--- Construtores
    
    //Construtor Default
    public BspNode(BspTree t) {
        //Nó folha
        front  = back = null;
        //Hiperplano
        plane  = null;
        //Tree
        tree   = t;
        //Nive
        level  = 0;
        //Back ?
        type   = BspConsts.CLASS_INVALID;
    }
    
    //Construtor a partir do pai
    public BspNode(BspNode father, int b) {
        //Nó folha
        front  = back = null;
        //Hiperplano
        plane  = null;
        //Tree
        tree   = father.tree;
        //Nivel
        level  = father.level + 1;
        //Back ?
        type   = b;
    }
        
    //--- Acessores 

    //retorna o nó front
    public BspNode getFront() {
        return front;
    }

    //retorna o nó back
    public BspNode getBack() {
        return back;
    }

    //retorna o plano
    public BspPlane getPlane() {
        return plane;
    }
    
    //retorna a árvore
    public BspTree getTree() {
        return tree;
    }    
    
    //retorna a árvore
    public int getLevel() {
        return level;
    }    
    
    //retorna a árvore
    public int getType() {
        return type;
    }

    //--- Atribuidores 
    
    //atribui o nó front
    public void setFront(BspNode f) {
        front = f;
    }

    //retorna o nó back
    public void setBack(BspNode b) {
        back = b;
    }

    //retorna o plano
    public void setPlane(BspPlane p) {
        plane = p;
    }
    
    //retorna a árvore
    public void setTree(BspTree t) {
        tree = t;
    }
    
    //--- Métodos
    
    //testa se o nó é folha
    boolean isLeaf(){
        return (front==null && back==null);
    }
    
    //--- Métodos abstratos
        
    //Divide o subespaço   
    public abstract void split(BspPlane splane, ArrayList<Object> objs, ArrayList<Object> frontObjs, ArrayList<Object> backObjs) ;
    
}