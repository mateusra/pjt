/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * class PolyBSP
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */
package ic.mlibs.structures;

//meus imports
import ic.mlibs.util.Helper;

//java imports
import java.util.ArrayList;

public class BspPoly {
    
    //número de pontos do poligono
    private int nPoints = 0; 
    //lista com a indice dos vértices do poligono
    private ArrayList<Integer> points = new ArrayList<Integer>(); 
    
    //--- Acessores e atribuidores
    
    //retorna o numero de pontos do poligono
    public int  getNpoints() {
        return nPoints;
    }
    
    //retorna o índice do iésimo vertice do poligono
    public int  getPoint(int i) {
        return points.get(i);
    }
    
    //adiciona um vértice a lista de poligonos
    public void addPoint(int i){
        points.add(i);
        nPoints++;
    }
        
    //Retorna o plano suporte do poligono
    public BspPlane PolyBSPtoPlaneBSP(BspTree tree) {
                
        //vértices do poligono
        Point3D p0 = tree.getG( getPoint(0) );
        Point3D p1 = tree.getG( getPoint(1) );
        Point3D p2 = tree.getG( getPoint(2) );
        
        //normal do poligono
        double[] n  = new double[3];
        //calcula a normal
        Helper.normal(p0, p1, p2, n);
        //cria um ponto 3D
        Point3D  nrm = new Point3D(n);
        nrm.getPos().normalize();
        //clacula o termo escalar
        double scalar = Helper.dot(p0,nrm);
        
        //retorna o plano
        return new BspPlane( nrm, scalar );        
    }
        
    //testa a posição de um ponto em relação ao plano
    public int polyToPlane(BspPlane p, BspTree tree) {
        
        //numero de vertices na frente e atras
        int nFront = 0, nBack = 0;
        //percorre os vertices
        for (int i = 0; i < getNpoints(); i++) {
            //pega a geometria do vertice
            Point3D pt = tree.getG( getPoint(i) );
            //classifica
            switch( p.pointToPlane(pt) ){
                case BspConsts.CLASS_FRONT: nFront++; break;
                case BspConsts.CLASS_BACK : nBack++ ; break;
            }
        }
        //leitura do resultado
        if(nFront != 0 && 
           nBack  != 0 ) return BspConsts.CLASS_CUT;   //> cruza o plano
        if(nFront != 0 ) return BspConsts.CLASS_FRONT; //> está na frente
        if(nBack  != 0 ) return BspConsts.CLASS_BACK;  //> está atras
        return BspConsts.CLASS_OVER;                   //> coplanar
    }

    
    //corta o poligono de acordo com o plano 
    public void split(BspPlane plane, BspPoly polyFront, BspPoly polyBack, BspTree tree) {
       
        //pega o numero de vértices do poligono atual
        int nvert = getNpoints();
        
        //Pega a última aresta primeiro
        int indexA = getPoint(nvert-1);
        int classA = plane.pointToPlane( tree.getG(indexA) );

        //percore as arestas do poligono 
        for (int n = 0; n < nvert; n++) {

            //Pega a última aresta primeiro
            int indexB = getPoint(n);
            int classB = plane.pointToPlane( tree.getG(indexB) );
            
            //B está na frente do plano
            if( classB == BspConsts.CLASS_FRONT ){
                //A está atras do plano
                if( classA == BspConsts.CLASS_BACK ){
                    //aresta (A,B) corta o plano
                    Point3D pointI = plane.intersectEdge(tree.getG(indexB), tree.getG(indexA)); 
                    
                     //Assegura que a intersessão está correta
                    if(pointI == null) { 
                        System.out.println("ERRO NULL"); 
                        continue; 
                    }
                   
                    //inclui o novo ponto na geometria da árvore
                    int indexI = tree.addG(pointI);
                    //adiciona os pontos aos poligonos
                    polyFront.addPoint(indexI);
                    polyBack .addPoint(indexI);
                }
                //Em todos os casos inclui B no poligono da frente
                polyFront.addPoint(indexB);
            }
            //B está atrás do plano
            else if( classB == BspConsts.CLASS_BACK ){
                //A está na frente do plano
                if( classA == BspConsts.CLASS_FRONT ){
                    //aresta (A,B) corta o plano
                    Point3D pointI = plane.intersectEdge(tree.getG(indexA), tree.getG(indexB)); 
                    
                    //Assegura que a intersessão está correta
                    if(pointI == null) { 
                        System.out.println("ERRO NULL");
                        continue;
                    }

                    //inclui o novo ponto na geometria da árvore
                    int indexI = tree.addG(pointI);
                    //adiciona os pontos aos poligonos
                    polyFront.addPoint(indexI);
                    polyBack .addPoint(indexI);
                }
                //A está sobre o plano
                else if( classA == BspConsts.CLASS_OVER ){
                    //A fica atrás do plano
                    polyBack.addPoint(indexA);
                }
                //Em todos os casos inclui B no poligono de trás
                polyBack.addPoint(indexB);
            }
            //B está sobre o plano
            else {
                //em todos os casos B fica na frente
                polyFront.addPoint(indexB);
                
                //em um dos casos também copia b para o Back
                if( classA == BspConsts.CLASS_BACK )
                    polyBack.addPoint(indexB);
            }
            
            //usa o B como ponto inicial da aresta
            indexA = indexB;
            classA = classB;
        }
    }
}
