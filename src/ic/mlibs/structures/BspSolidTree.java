/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * class PcBspTree
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */
package ic.mlibs.structures;

//meus imports
import ic.mlibs.gl.GLMesh;

//java imports
import java.util.ArrayList;

public class BspSolidTree extends BspTree {

    GLMesh mesh; //> Malha guia da BSP
    
    public BspSolidTree( GLMesh m ){
        //construtor da superclass
        super();
        
        //aloca o root
        root = new BspSolidNode( this );
        
        //cópia da geometria
        buildGeometry(m);
    } 
    
    //Testa se um ponto está dentro da BSP
    public int pointInSolid( BspNode node, Point3D p ) {
               
        //enquanto o nó não é folha
        while( !node.isLeaf() ) {
            //testa se está na frente do hyperplano
            int c = node.getPlane().pointToPlane(p);

            //atualiza o nó
            if     (c == BspConsts.CLASS_FRONT) node=node.getFront();
            else if(c == BspConsts.CLASS_BACK ) node=node.getBack();
            else {
                int resFront = pointInSolid(node.getFront(),p); 
                int resBack  = pointInSolid(node.getBack() ,p); 
                
                if(resBack == resFront) return resBack;
                else return BspConsts.CLASS_OVER;
            }
        }
        //retorna o resultado
        return node.getType();
    }
    
    @Override
    protected void buildGeometry(Object o)  {
         
        GLMesh mesh = (GLMesh) o;
        
        //copia os vértices
        for (int i = 0; i < mesh.getNvert(); i++) {
            G.add( mesh.getG(i) );            
        }
        
        //copia as faces triangulares
        for (int i = 0; i < mesh.getNtrig(); i++) {
            int base = mesh.base(i);
            
            BspPoly p = new BspPoly();            
            p.addPoint( mesh.getV( base++ ) );
            p.addPoint( mesh.getV( base++ ) );
            p.addPoint( mesh.getV( base++ ) );
                        
            P.add(p);
        }

        //copia as faces quadrangulares
        for (int i = mesh.getNtrig(); i < mesh.getNface(); i++) {
            int base   = mesh.base(i);
            int baseCp = mesh.base(i);
            
            BspPoly p1 = new BspPoly();            
            p1.addPoint( mesh.getV( base++ ) );
            p1.addPoint( mesh.getV( base++ ) );
            p1.addPoint( mesh.getV( base   ) );
            P.add(p1);
            
            BspPoly p2 = new BspPoly();            
            p2.addPoint( mesh.getV( base++ ) );
            p2.addPoint( mesh.getV( base   ) );
            p2.addPoint( mesh.getV( baseCp ) );
            P.add(p2);
        }       
    }
    
    //escolhe um bom plano para subdividir a bsp
    @Override
    protected BspPlane chooseBestPlane(ArrayList<Object> objs) {
        
        BspPoly poly = (BspPoly)objs.get(0);
        
        BspPlane hPlane = poly.PolyBSPtoPlaneBSP(this);          
        //retira o poligono da lista
        objs.remove(0);
        
        return hPlane;
    }    
}
