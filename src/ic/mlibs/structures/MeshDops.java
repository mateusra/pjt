/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe MeshDops
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.structures;

//Java
import ic.mlibs.util.Helper;
import java.util.ArrayList;

public class MeshDops extends MeshTops implements CheDops{

    @Override
    public double minCurvature(int vid) {
        double media =  meanCurvature(vid);
        double gauss = gaussCurvature(vid);
        
        double curvature = (double) ( media - Math.sqrt(media*media - gauss) ); 

        return curvature;
    }

    @Override
    public double maxCurvature(int vid) {
        double media =  meanCurvature(vid);
        double gauss = gaussCurvature(vid);
        
        double curvature = (double) ( media + Math.sqrt(media*media - gauss) );

        return curvature;
    }

    @Override
    public double meanCurvature(int vid) {
        
        //Estrela do vértice
        ArrayList<Integer> star = R_00(vid);
        
        //Centro da estrela
        final Point3D p = getG(vid);
        
        //Curvatura Média
        double curvature = 0;
        double area = 0;
        
        for (int i = 0; i < star.size(); i++) {
            //Pega os vértices da estrela
            final Point3D p0 = getG( star.get(( i )%star.size()) );
            final Point3D p1 = getG( star.get((i+1)%star.size()) );
            final Point3D p2 = getG( star.get((i+2)%star.size()) );
           
            //área do triângulo
            area += Helper.area(p,p0,p1);
            
            //angulo entre vetores
            double argA = Helper.angle(p0, p1, p);
            double argB = Helper.angle(p2, p1, p);
            
            //cotangentes
            double cotA = (double) (Math.cos(argA) / Math.sin(argA)) ;
            double cotB = (double) (Math.cos(argB) / Math.sin(argB)) ;
            
            //Acumula na curvatura
            curvature += (cotA+cotB)*( Helper.sub( p,p1 ) ).getPos().size();
        }
 
        return curvature / (2*area);
    }

    @Override
    public double gaussCurvature(int vid) {
        
        //Estrela do vértice
        ArrayList<Integer> star = R_00(vid);
        
        //Centro da estrela
        final Point3D p = getG(vid);
        
        //Curvatura Gaussiana
        double curvature = (double) (2.0f*Math.PI);
        double area = 0;
        
        for (int i = 0; i < star.size(); i++) {
            //Pega os vértices da estrela
            final Point3D p0 = getG( star.get(( i )%star.size()) );
            final Point3D p1 = getG( star.get((i+1)%star.size()) );
            
            //área do triângulo
            area += Helper.area(p,p0,p1);
            
            //angulo entre vetores
            double angle = Helper.angle(p, p0, p1);
            
            //Acumula na curvatura
            curvature -= angle;
        }
 
        return curvature / area;
    }

    @Override
    public double laplacian(int vid, double [] function) {
        
        //Estrela do vértice
        ArrayList<Integer> star = R_00(vid);
        
        //Centro da estrela
        final Point3D p = getG(vid);
        
        //Laplaciano
        double laplacian = 0;
        double area = 0;
        double fp = function[vid];
        
        for (int i = 0; i < star.size(); i++) {
            //Pega os vértices da estrela
            final Point3D p0 = getG( star.get(( i )%star.size()) );
            final Point3D p1 = getG( star.get((i+1)%star.size()) );
            final Point3D p2 = getG( star.get((i+2)%star.size()) );
            
            double fp1 = function[ star.get(( i )%star.size()) ];
            
            //área do triângulo
            area += Helper.area(p,p0,p1);
            
            //angulo entre vetores
            double argA = Helper.angle(p0, p1, p);
            double argB = Helper.angle(p2, p1, p);
            
            //cotangentes
            double cotA = (double) (Math.cos(argA) / Math.sin(argA)) ;
            double cotB = (double) (Math.cos(argB) / Math.sin(argB)) ;
            
            //Acumula o laplaciano
            laplacian += (cotA+cotB)*( fp1 - fp );
        }
 
        return laplacian / (2*area);
    }    
}
