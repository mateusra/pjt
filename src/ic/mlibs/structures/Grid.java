/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * class Grid
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.mlibs.structures;

//java imports
import ic.apps.mateus.Particula;
import ic.mlibs.util.Helper;
import java.util.ArrayList;

public abstract class Grid {
    
    //resolução do grid em X
    protected int resX = 0;
    //resolução do grid em Y
    protected int resY = 0;
    //resolução do grid em Z
    protected int resZ = 0;

    //tamanho de uma célula
    protected double sizeC = 0;
    
    //Grid de busca
    protected ArrayList<GridCell> grid = new ArrayList<GridCell>();
    
    //---Construtor
    
    public Grid( double szC ){
        //define o tamanho da célula
        sizeC = szC;
        //inicializa o grid em (-1,-1,-1)
        buildGrid( new Point3D(-1.0f,-1.0f,-1.0f), new Point3D(1,1,1));
    }
    
    public Grid( Point3D min, Point3D max, double szC ){
        //define o tamanho da célula
        sizeC = szC;
        //inicializa o grid em (-1,-1,-1)
        buildGrid(min, max);
    }
    
    //---Acessores
    
    public int [] XyzToIjk(double x, double y, double z){
        
        Point3D min = Helper.sub( grid.get(0).getPos(), new Point3D(sizeC/2.0f,sizeC/2.0f,sizeC/2.0f) );
        
        int i = (int) Math.floor( (-min.getPosX() + x) / sizeC  );
        int j = (int) Math.floor( (-min.getPosY() + y) / sizeC  );
        int k = (int) Math.floor( (-min.getPosZ() + z) / sizeC  );
        
        int [] res = {i,j,k};
        
        return res;

    }
    
    //retorna uma célula pela pos (x,y,z)
    public GridCell getGridCell(double x, double y, double z) {
        
        Point3D min = Helper.sub( grid.get(0).getPos(), new Point3D(sizeC/2.0f,sizeC/2.0f,sizeC/2.0f) );
        
        int i = (int) Math.floor( (-min.getPosX() + x) / sizeC  );
        int j = (int) Math.floor( (-min.getPosY() + y) / sizeC  );
        int k = (int) Math.floor( (-min.getPosZ() + z) / sizeC  );
        
        //indices impossíveis
        if( i<0 || i>=resX ) return null;  
        if( j<0 || j>=resY ) return null;  
        if( k<0 || k>=resZ ) return null;  
        
        return getGridCell(i,j,k);        
    }
    
    //retorna uma célula pela id (i,j,k)
    public GridCell getGridCell(int i, int j, int k) {
        
        int id = i + j*resX+ k*(resX*resY);
        return getGridCell(id);        
    }    

    //retorna uma célula pela id linear
    public GridCell getGridCell(int i) {
        return grid.get(i);
    }

    //retorna a lista de células
    public ArrayList<GridCell> getGrid() {
        return grid;
    }

    //retorna a resolução em X
    public int getResX() {
        return resX;
    }

    //retorna a resolução em Y
    public int getResY() {
        return resY;
    }

    //retorna a resolução em Z
    public int getResZ() {
        return resZ;
    }

    //retorna o tamanho de uma célula
    public double getSizeC() {
        return sizeC;
    }
    
    //---Métodos
    
    public ArrayList<Particula> getNeighs( double x, double y, double z ){
        
        //Pega a célula 
        int [] ids = XyzToIjk(x,y,z);
               
        return getNeighs(ids[0],ids[1],ids[2]);
    }
    
    public ArrayList<Particula> getNeighs( int i, int j, int k ){
        
        //ArrayList de retorno
        ArrayList<Particula> neighs = new ArrayList<>();
        
        //indice inicial
        int i0 = (i-1 <   0   )?(0):(i-1);
        int j0 = (j-1 <   0   )?(0):(j-1);
        int k0 = (k-1 <   0   )?(0):(k-1);
        
        //indice final
        int i1 = (i+1 >= resX)?(resX-1):(i+1);
        int j1 = (j+1 >= resY)?(resY-1):(j+1);
        int k1 = (k+1 >= resZ)?(resZ-1):(k+1);
        
        //percorre as células
        for (int ic = i0; ic <= i1; ic++) {
            for (int jc = j0; jc <= j1; jc++) {
                for (int kc = k0; kc <= k1; kc++) {
                    GridCell c = getGridCell(ic, jc, kc);
                    
                    ArrayList<Particula> currNeighs = c.getParts();
                    neighs.addAll(currNeighs);
                }
            }
        }
        
        return neighs;
    }
        
    public void buildNeighs() {

        System.out.print("Grid::buildNeighs -->");

        //Percorre o grid
        for (int i = 0; i < getResX(); i++) {
            for (int j = 0; j < getResY(); j++) {
                for (int k = 0; k < getResZ(); k++) {
                    //copia a celula
                    GridCell cel = getGridCell(i,j,k); 
                    
                    //possiveis vizinhos
                    ArrayList<Particula> candidates = getNeighs(i,j,k);
                    
                    //atribui os vizinhos
                    cel.setNeigs(candidates);
                }
            }
        }   

        System.out.println("Sucesso!");
    }
    
    //---Métodos Protegidos

    protected void buildGrid(Point3D min, Point3D max) {
        
        System.out.print("Grid::buildGrid -->");

        //Lados do grid
        Point3D sides = Helper.sub(max, min);
        
        //Numero de celulas em cada direção
        resX = (int)Math.ceil(  sides.getPosX() / sizeC  )+2;
        resY = (int)Math.ceil(  sides.getPosY() / sizeC  )+2;
        resZ = (int)Math.ceil(  sides.getPosZ() / sizeC  )+2;
        
        //evita grids de uma célula
        if(resX == 1 || resY == 1 || resZ == 1){
            System.out.println("SphGrid::initialize: Erro na resolução do grid !");
            return;
        }
                
        //poontos
        Point3D atual  = min;
        Point3D deltaX = new Point3D( sizeC,0,0 );
        Point3D deltaY = new Point3D( 0,sizeC,0 );
        Point3D deltaZ = new Point3D( 0,0,sizeC );
        
        //Pecorre Z
        for (int k = 0; k < resZ; k++) {
            //percorre Y
            for (int j = 0; j < resY; j++) {
                //percorre X
                for (int i = 0; i < resX; i++) {
                    //adiciona o ponto atual
                    grid.add( new GridCell( atual ) );
                    //incrementa X
                    atual = Helper.add(atual, deltaX);
                }
                //volta com X do min
                atual.setPosX(min.getPosX());
                //Incrementa  Y
                atual = Helper.add(atual, deltaY);              
            }
            //volta com Y do min
            atual.setPosY(min.getPosY());
            //Incrementa  Z
            atual = Helper.add(atual, deltaZ);
        }

        System.out.println("Sucesso!");
    }
    
    //---Métodos abstratos
    
    public abstract void insertObject( Object o );
}
