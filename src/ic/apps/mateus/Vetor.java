package ic.apps.mateus;

/**
 * Created by mateusra on 16/02/14.
 */
public class Vetor {

    private double componenteX, componenteY, modulo, angulo;

    public Vetor(double componenteX, double componenteY) {
        this.componenteX = componenteX;
        this.componenteY = componenteY;
        this.modulo = Math.sqrt(Math.pow(componenteX, 2) + Math.pow(componenteY, 2));
        this.angulo = Math.atan(componenteY/componenteX);
    }

    public Vetor(double modulo) {
        this.modulo = Math.abs(modulo);
        this.angulo = 0;
        this.componenteX = modulo;
        this.componenteY = 0;
    }

    public void setComponenteX(double componenteX) {
        this.componenteX = componenteX;
        this.modulo = Math.sqrt(Math.pow(componenteX, 2) + Math.pow(componenteY, 2));
        this.angulo = Math.atan(componenteY/componenteX);
    }

    public void setComponenteY(double componenteY) {
        this.componenteY = componenteY;
        this.modulo = Math.sqrt(Math.pow(componenteX, 2) + Math.pow(componenteY, 2));
        this.angulo = Math.atan(componenteY/componenteX);
    }

    public void setModulo(double modulo) {
        this.modulo = Math.abs(modulo);
        this.componenteX = modulo * Math.cos(angulo);
        this.componenteY = modulo * Math.sin(angulo);
    }

    public void setAngulo(double angulo) {
        this.angulo = Math.toRadians(angulo);
        this.componenteX = modulo * Math.cos(angulo);
        this.componenteY = modulo * Math.sin(angulo);
    }

    public double getComponenteX() {
        return componenteX;
    }

    public double getComponenteY() {
        return componenteY;
    }

    public double getModulo() {
        return modulo;
    }

    public double getAngulo() {
        return Math.toDegrees(angulo);
    }

    public double produtoEscalar(Vetor v){
        return this.componenteX*v.getComponenteX()+this.componenteY*v.getComponenteY();
    }

    public Vetor somaVetoral(Vetor v){
        return  new Vetor(this.componenteX + v.getComponenteX(),this.componenteY + v.getComponenteY());
    }
}
