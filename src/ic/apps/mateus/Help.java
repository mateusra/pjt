package ic.apps.mateus;

import ic.mlibs.structures.GridCell;
import ic.mlibs.structures.Point3D;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import org.lwjgl.opengl.GL11;
import java.util.regex.Pattern;
import org.lwjgl.LWJGLException;

public class Help {

    public static double[] Gauss(double[][] coefiecientesOriginal, double[] constantes) {
        int n = coefiecientesOriginal.length - 1;
        double[][] coefiecientes = coefiecientesOriginal.clone();
        for (int k = 0; k < n; k++) {
            int maior = k;
            for (int i = k + 1; i <= n; i++) {
                if (Math.abs(coefiecientes[i][k]) > Math.abs(coefiecientes[maior][k])) {
                    maior = i;
                }
            }
            if (maior != k) {
                double[] temp = coefiecientes[k];
                coefiecientes[k] = coefiecientes[maior];
                coefiecientes[maior] = temp;
                double t = constantes[k];
                constantes[k] = constantes[maior];
                constantes[maior] = t;
            }
            for (int i = k + 1; i <= n; i++) {
                double divisor = coefiecientes[i][k] / coefiecientes[k][k];
                coefiecientes[i][k] = 0;
                for (int j = k + 1; j <= n; j++) {
                    coefiecientes[i][j] = coefiecientes[i][j] - divisor * coefiecientes[k][j];
                }
                constantes[i] -= divisor * constantes[k];
            }
        }
        double[] solucaoAprox = new double[coefiecientes.length];
        solucaoAprox[n] = constantes[n] / coefiecientes[n][n];
        for (int l = n - 1; l >= 0; l--) {
            double soma = 0;
            for (int j = l + 1; j <= n; j++) {
                soma += solucaoAprox[j] * coefiecientes[l][j];
            }
            solucaoAprox[l] = (constantes[l] - soma) / coefiecientes[l][l];
        }
        return solucaoAprox;
    }

    public static double[] somaVetor(double[] a, double[] b) {
        double[] resp = new double[a.length];
        for (int i = 0; i < resp.length; i++) {
            resp[i] = a[i] + b[i];
        }
        return resp;
    }

    public static double[] matrizParaLinha(double[][] matriz) {
        double[] linha = new double[matriz.length * matriz[0].length];
        int posicao = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                linha[posicao] = matriz[i][j];
                posicao++;
            }
        }
        return linha;
    }

    public static double[][] linhaParaMatriz(double[] linha, int numlinhas) {
        double[][] matriz = new double[numlinhas][linha.length / numlinhas];
        int posicao = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = linha[posicao];
                posicao++;
            }
        }
        return matriz;
    }

    public static void escreveVetor(double[] vetor, String nome) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(nome));
        for (int i = 0; i < vetor.length; i++) {
            System.out.printf("%1$,.0f ", vetor[i]);

        }
        System.out.println("");
//        for (double elemento : vetor) {
//            out.write(String.format("%1$,.2f", elemento) + "\r\n");
//        }
        out.close();
    }

    public static void escreveMatriz(double[][] matriz, String nome) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(nome));
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.printf("%1$,.0f ", matriz[i][j]);
            }
            System.out.println("");
        }
//        for (double[] linha : matriz) {
//            for (double elemento : linha) {
//                //out.write(String.format("%1$,.0f", elemento) + " ");
//                System.out.printf("%1$,.0f ", elemento);
//            }
//            //out.write("\r\n");
//            System.out.println("");
//        }
        out.close();
    }

    public static double[] concat(double[] a, double[] b) throws IOException {
        double[] resp = new double[a.length + b.length];
        int i;
        for (i = 0; i < a.length; i++) {
            resp[i] = a[i];
        }

        for (i = 0; i < resp.length; i++) {
            resp[i] = b[i - a.length];
        }
        return resp;
    }

    public static double[] Newton(double[][] jacob, double erro, double[] f) {
        double[] x0 = new double[jacob.length];
        double[] x1 = new double[jacob.length];
        double max = max(x0, x1);
        while (max < erro) {
            x0 = x1.clone();
            x1 = somaVetor(x0, Gauss(jacob, f));
            max = max(x0, x1);
            if (max < erro) {
                break;
            }
        }
        return x1;
    }

    public static double max(double[] a, double[] b) {
        double maior = Math.abs(Math.abs(a[0]) - Math.abs(b[0]));
        for (int i = 0; i < a.length; i++) {
            if (maior < Math.abs(Math.abs(a[i]) - Math.abs(b[i]))) {
                maior = Math.abs(Math.abs(a[i]) - Math.abs(b[i]));
            }
        }
        return maior;
    }

    public static void drawVector(double x, double y, double lado, double modulo, double angle, double maiorModulo) {
        double max;
        if (angle != 90 && angle != 270 && angle != -90 && angle != -270) {
            max = Math.abs(lado / Math.cos(Math.toRadians(angle)));
        } else {
            max = Math.abs(lado / Math.sin(Math.toRadians(angle)));
        }
        max = max * (modulo / maiorModulo);
        double xMax = x + max * Math.cos(Math.toRadians(angle));
        double yMax = y + max * Math.sin(Math.toRadians(angle));
        GL11.glLineWidth(2);
        GL11.glColor3d(1, 0, 0);
        GL11.glBegin(GL11.GL_LINES);
        GL11.glVertex2d(x, y);
        GL11.glVertex2d(xMax, yMax);
        GL11.glEnd();
        double aux = 5;
        GL11.glBegin(GL11.GL_TRIANGLES);
        GL11.glVertex2d(xMax, yMax);
        GL11.glVertex2d(x + max * Math.cos(Math.toRadians(angle + aux)) * 0.9, x + max * Math.sin(Math.toRadians(angle + aux)) * 0.9);
        GL11.glVertex2d(x + max * Math.cos(Math.toRadians(angle - aux)) * 0.9, x + max * Math.sin(Math.toRadians(angle - aux)) * 0.9);
        GL11.glEnd();
    }
}
