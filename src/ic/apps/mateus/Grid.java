package ic.apps.mateus;

import java.io.IOException;

public class Grid {

    private double dx, dy, dt, viscosity, rho, Re;
    private Cell[][] grid;
    private Cell min, max;

    public double getRe() {
        return Re;
    }

    public void setRe(double Re) {
        this.Re = Re;
    }

    public Grid(Cell min, Cell max, double ds) {
        grid = new Cell[(int) ((max.getPositionY() - min.getPositionY()) / ds) + 1][(int) ((max.getPositionX() - min.getPositionX()) / ds) + 1];
        double y = min.getPositionY();
        for (int i = 0; i < grid.length; i++) {
            double x = min.getPositionX();
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = new Cell(x, y);
                x += ds;
            }
            y += ds;
        }
        startCond();
        this.dx = this.dy = ds;
    }

    public Grid(Cell min, Cell max, int nx, int ny) {
        grid = new Cell[nx][ny];
        this.dx = (max.getPositionX() - min.getPositionX()) / nx;
        this.dy = (max.getPositionY() - min.getPositionY()) / ny;
        double y = min.getPositionY();
        for (int i = 0; i < grid.length; i++) {
            double x = min.getPositionX();
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == null) {
                    grid[i][j] = new Cell(x, y);
                }
                x += dx;
            }
            y += dy;
        }
        startCond();
    }

    public double getDx() {
        return dx;
    }

    public void setDx(double dx) {
        Re = (dx*grid[0].length*rho*grid[grid.length-1][0].getVelocityX())/viscosity;
        this.dx = dx;
    }

    public double getDy() {
        return dy;
    }

    public void setDy(double dy) {
        Re = (dx*grid[0].length*rho*grid[grid.length-1][0].getVelocityX())/viscosity;
        this.dy = dy;
    }

    public double getRho() {
        return rho;
    }

    public void setRho(double rho) {
        Re = (dx*grid[0].length*rho*grid[grid.length-1][0].getVelocityX())/viscosity;
        this.rho = rho;
    }

    public double getViscosity() {
        return this.viscosity;
    }

    public void setViscosity(double viscosity) {
        Re = (dx*grid[0].length*rho*grid[grid.length-1][0].getVelocityX())/viscosity;
        this.viscosity = viscosity;
    }

    public double getTime() {
        return this.dt;
    }

    public void setTime(double dt) {
        this.dt = dt;
    }

    public Cell getCell(double x, double y) {
        int i = (int) (x - min.getPositionX()), j = (int) (y - min.getPositionY());
        return (i >= 0 && j >= 0 && i < grid.length && j < grid[0].length) ? grid[i][j] : null;
    }

    public Cell[][] getGrid() {
        return grid;
    }

    public void cavityFlow(int modo) {
        double[][] gridVelocidadeX = new double[grid.length][grid[0].length], gridVelocidadeY = new double[grid.length][grid[0].length], gridpressao = new double[grid.length][grid[0].length];
        final Cell[][] clone = grid.clone();

        for (int i = 1; i < gridpressao.length - 1; i++) {
            gridpressao[i] = this.pressurePoissonSim(i);
            gridVelocidadeX[i] = this.momentumXSim(i);
            gridVelocidadeY[i] = this.momentumYSim(i);
        }

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j].setVelocityX(gridVelocidadeX[i][j]);
                grid[i][j].setVelocityY(gridVelocidadeY[i][j]);
                grid[i][j].setPressure(gridpressao[i][j]);
            }
        }
        grid = clone;
        this.setBoundary(modo);
    }

    private double[] momentumX(int linha) {
        double[] lGrid = new double[grid[linha].length];
        for (int j = 1; j < grid[linha].length - 1; j++) {
            double udx2dy2 = (dt / (dx * dx)) * (grid[linha][j - 1].getVelocityX() - 2 * grid[linha][j].getVelocityX() + grid[linha][j + 1].getVelocityX()) + (dt / (dy * dy)) * (grid[linha - 1][j].getVelocityX() - 2 * grid[linha][j].getVelocityX() + grid[linha + 1][j].getVelocityX());
            double u2dx = grid[linha][j].getVelocityX() * (grid[linha][j].getVelocityX() - grid[linha][j - 1].getVelocityX()) * (dt / dx);
            double vudy = grid[linha][j].getVelocityY() * (grid[linha][j].getVelocityX() - grid[linha - 1][j].getVelocityX()) * (dt / dy);
            double pdx = (grid[linha][j + 1].getPressure() - grid[linha][j - 1].getPressure()) * (dt / (2 * rho * dx));

            lGrid[j] = grid[linha][j].getVelocityX() - u2dx - vudy - pdx + viscosity * udx2dy2;
        }
        return lGrid;
    }

    private double[] momentumXSim(int linha) {
        double[] lGrid = new double[grid[linha].length];
        for (int j = 1; j < grid[linha].length - 1; j++) {
            lGrid[j] = grid[linha][j].getVelocityX() - dt / (2 * dx) * (grid[linha][j].getVelocityX() * (grid[linha][j + 1].getVelocityX() - grid[linha][j - 1].getVelocityX())
                    + grid[linha][j].getVelocityY() * (grid[linha + 1][j].getVelocityX() - grid[linha - 1][j].getVelocityX()) + (grid[linha][j + 1].getPressure() - grid[linha][j - 1].getPressure()) / rho)
                    + (viscosity * dt) / (dx * dx) * (grid[linha - 1][j].getVelocityX() + grid[linha][j - 1].getVelocityX() - 4 * grid[linha][j].getVelocityX() + grid[linha][j + 1].getVelocityX() + grid[linha + 1][j].getVelocityX());
        }
        return lGrid;
    }

    private double[] momentumY(int linha) {
        double[] lGrid = new double[grid[linha].length];
        for (int j = 1; j < grid[linha].length - 1; j++) {
            double vdx2dy2 = (dt / (dx * dx)) * (grid[linha][j - 1].getVelocityY() - 2 * grid[linha][j].getVelocityY() + grid[linha][j + 1].getVelocityY()) + (dt / (dy * dy)) * (grid[linha - 1][j].getVelocityY() - 2 * grid[linha][j].getVelocityY() + grid[linha + 1][j].getVelocityY());
            double uvdx = grid[linha][j].getVelocityX() * (grid[linha][j].getVelocityY() - grid[linha][j - 1].getVelocityY()) * (dt / dx);
            double v2dy = grid[linha][j].getVelocityY() * (grid[linha][j].getVelocityY() - grid[linha - 1][j].getVelocityY()) * (dt / dy);
            double pdy = (grid[linha + 1][j].getPressure() - grid[linha - 1][j].getPressure()) * (dt / (2 * rho * dy));

            lGrid[j] = grid[linha][j].getVelocityY() - uvdx - v2dy - pdy + viscosity * vdx2dy2;
        }
        return lGrid;
    }

    private double[] momentumYSim(int linha) {
        double[] lGrid = new double[grid[linha].length];
        for (int j = 1; j < grid[linha].length - 1; j++) {
            lGrid[j] = grid[linha][j].getVelocityY() - dt / (2 * dx) * (grid[linha][j].getVelocityX() * (grid[linha][j + 1].getVelocityY() - grid[linha][j - 1].getVelocityY())
                    + grid[linha][j].getVelocityY() * (grid[linha - 1][j].getVelocityY() - grid[linha - 1][j].getVelocityY()) + (grid[linha + 1][j].getPressure() - grid[linha - 1][j].getPressure()) / rho)
                    + (viscosity * dt) / (dx * dx) * (grid[linha - 1][j].getVelocityY() + grid[linha][j - 1].getVelocityY() - 4 * grid[linha][j].getVelocityY() + grid[linha][j + 1].getVelocityY() + grid[linha + 1][j].getVelocityY());
        }
        return lGrid;
    }

    private double[] pressurePoisson(int linha) {
        double[] lGrid = new double[grid[linha].length];
        for (int j = 1; j < grid[linha].length - 1; j++) {
            double mpp = ((grid[linha][j + 1].getPressure() + grid[linha][j - 1].getPressure()) * dy * dy + (grid[linha + 1][j].getPressure() + grid[linha - 1][j].getPressure()) * dx * dx) / (2 * (dx * dx + dy * dy));
            double k = (rho * dx * dx * dy * dy) / (2 * (dx * dx + dy * dy));
            double udx = (grid[linha][j + 1].getVelocityX() - grid[linha][j - 1].getVelocityX()) / (2 * dx);
            double udy = (grid[linha + 1][j].getVelocityX() - grid[linha - 1][j].getVelocityX()) / (2 * dy);
            double vdx = (grid[linha][j + 1].getVelocityY() - grid[linha][j - 1].getVelocityY()) / (2 * dx);
            double vdy = (grid[linha + 1][j].getVelocityY() - grid[linha - 1][j].getVelocityY()) / (2 * dy);

            lGrid[j] = mpp - k * ((udx + vdy) / dt - udx * udx - 2 * udy * vdx - vdy * vdy);
        }
        return lGrid;
    }

    private double[] pressurePoissonSim(int linha) {
        double[] lGrid = new double[grid[linha].length];
        for (int j = 1; j < grid[linha].length - 1; j++) {
            lGrid[j] = (grid[linha - 1][j].getPressure() + grid[linha][j - 1].getPressure() + grid[linha + 1][j].getPressure() + grid[linha][j + 1].getPressure()) / 4
                    - rho * dx / 8 * ((grid[linha][j + 1].getVelocityX() - grid[linha][j - 1].getVelocityX() + grid[linha + 1][j].getVelocityY() - grid[linha - 1][j].getVelocityY()) / dt
                    - (Math.pow(grid[linha][j + 1].getVelocityX() - grid[linha][j - 1].getVelocityX(), 2) + 2 * (grid[linha + 1][j].getVelocityX() - grid[linha - 1][j].getVelocityX()) * (grid[linha][j + 1].getVelocityY() - grid[linha][j - 1].getVelocityY())
                    + Math.pow((grid[linha + 1][j].getVelocityY() - grid[linha - 1][j].getVelocityY()), 2)) / (2 * dx));
        }
        return lGrid;
    }

    public void startCond() {
        double dp = 1d / grid.length;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j].setVelocityX(0);
                grid[i][j].setVelocityY(0);
                grid[i][j].setPressure(0);
            }
        }
        this.setBoundary(0);
    }

    public void setBoundary(int modo) {
        switch (modo) {
            case 0:
                //linha de baixo
                for (int i = 0; i < grid[0].length; i++) {
                    grid[0][i].setVelocityX(0);
                    grid[0][i].setVelocityY(0);
                    grid[0][i].setPressure(grid[1][i].getPressure());
                }
                //coluna direita
                for (int i = 0; i < grid.length; i++) {
                    grid[i][0].setVelocityX(0);
                    grid[i][0].setVelocityY(0);
                    grid[i][0].setPressure(grid[i][1].getPressure());
                }
                //coluna esquerda
                for (int i = 0; i < grid.length; i++) {
                    grid[i][grid[i].length - 1].setVelocityX(0);
                    grid[i][grid[i].length - 1].setVelocityY(0);
                    grid[i][grid[i].length - 1].setPressure(grid[i][grid[i].length - 2].getPressure());
                }
                //linha acima
                for (int i = 0; i < grid[0].length; i++) {
                    grid[grid.length - 1][i].setVelocityX(0);
                    grid[grid.length - 1][i].setVelocityY(0);
                    grid[grid.length - 1][i].setPressure(grid[grid.length - 2][i].getPressure());
                }
                break;
            case 1:
                //linha de baixo
                for (int i = 0; i < grid[0].length; i++) {
                    grid[0][i].setVelocityX(0);
                    grid[0][i].setVelocityY(0);
                    grid[0][i].setPressure(grid[1][i].getPressure());
                }
                //coluna direita
                for (int i = 0; i < grid.length; i++) {
                    grid[i][0].setVelocityX(0);
                    grid[i][0].setVelocityY(0);
                    grid[i][0].setPressure(grid[i][1].getPressure());
                }
                //coluna esquerda
                for (int i = 0; i < grid.length; i++) {
                    grid[i][grid[i].length - 1].setVelocityX(0);
                    grid[i][grid[i].length - 1].setVelocityY(0);
                    grid[i][grid[i].length - 1].setPressure(grid[i][grid[i].length - 2].getPressure());
                }
                //linha acima
                for (int i = 0; i < grid[0].length; i++) {
                    grid[grid.length - 1][i].setVelocityX(1);
                    grid[grid.length - 1][i].setVelocityY(0);
                    grid[grid.length - 1][i].setPressure(0);
                }
                break;
            case 2:
                //linha de baixo
                for (int i = 0; i < grid[0].length; i++) {
                    grid[0][i].setVelocityX(0);
                    grid[0][i].setVelocityY(0);
                    grid[0][i].setPressure(grid[1][i].getPressure());
                }
                //coluna direita
                for (int i = 0; i < grid.length; i++) {
                    grid[i][0].setVelocityX(0);
                    grid[i][0].setVelocityY(0);
                    grid[i][0].setPressure(grid[i][1].getPressure());
                }
                //coluna esquerda
                for (int i = 0; i < grid.length; i++) {
                    grid[i][grid[i].length - 1].setVelocityX(0);
                    grid[i][grid[i].length - 1].setVelocityY(0);
                    grid[i][grid[i].length - 1].setPressure(grid[i][grid[i].length - 2].getPressure());
                }
                //linha acima
                for (int i = 0; i < grid[0].length; i++) {
                    grid[grid.length - 1][i].setVelocityX(10);
                    grid[grid.length - 1][i].setVelocityY(0);
                    grid[grid.length - 1][i].setPressure(0);
                }
                break;
            case 3:
                //linha de baixo
                for (int i = 0; i < grid[0].length; i++) {
                    grid[0][i].setVelocityX(-10);
                    grid[0][i].setVelocityY(0);
                    grid[0][i].setPressure(1);
                }
                //coluna direita
                for (int i = 0; i < grid.length; i++) {
                    grid[i][0].setVelocityX(0);
                    grid[i][0].setVelocityY(10);
                    grid[i][0].setPressure(grid[i][1].getPressure());
                }
                //coluna esquerda
                for (int i = 0; i < grid.length; i++) {
                    grid[i][grid[i].length - 1].setVelocityX(0);
                    grid[i][grid[i].length - 1].setVelocityY(-10);
                    grid[i][grid[i].length - 1].setPressure(grid[i][grid[i].length - 2].getPressure());
                }
                //linha acima
                for (int i = 0; i < grid[0].length; i++) {
                    grid[grid.length - 1][i].setVelocityX(10);
                    grid[grid.length - 1][i].setVelocityY(0);
                    grid[grid.length - 1][i].setPressure(0);
                }
                break;
            case 4:
                //linha de baixo
                for (int i = 0; i < grid[0].length; i++) {
                    grid[0][i].setVelocityX(10);
                    grid[0][i].setVelocityY(0);
                    grid[0][i].setPressure(1);
                }
                //coluna direita
                for (int i = 0; i < grid.length; i++) {
                    grid[i][0].setVelocityX(0);
                    grid[i][0].setVelocityY(0);
                    grid[i][0].setPressure(grid[i][1].getPressure());
                }
                //coluna esquerda
                for (int i = 0; i < grid.length; i++) {
                    grid[i][grid[i].length - 1].setVelocityX(0);
                    grid[i][grid[i].length - 1].setVelocityY(0);
                    grid[i][grid[i].length - 1].setPressure(grid[i][grid[i].length - 2].getPressure());
                }
                //linha acima
                for (int i = 0; i < grid[0].length; i++) {
                    grid[grid.length - 1][i].setVelocityX(10);
                    grid[grid.length - 1][i].setVelocityY(0);
                    grid[grid.length - 1][i].setPressure(0);
                }
                break;
            case 5:
                //linha de baixo
                for (int i = 0; i < grid[0].length; i++) {
                    grid[0][i].setVelocityX(-10);
                    grid[0][i].setVelocityY(0);
                    grid[0][i].setPressure(1);
                }
                //coluna direita
                for (int i = 0; i < grid.length; i++) {
                    grid[i][0].setVelocityX(0);
                    grid[i][0].setVelocityY(-10);
                    grid[i][0].setPressure(grid[i][1].getPressure());
                }
                //coluna esquerda
                for (int i = 0; i < grid.length; i++) {
                    grid[i][grid[i].length - 1].setVelocityX(0);
                    grid[i][grid[i].length - 1].setVelocityY(10);
                    grid[i][grid[i].length - 1].setPressure(grid[i][grid[i].length - 2].getPressure());
                }
                //linha acima
                for (int i = 0; i < grid[0].length; i++) {
                    grid[grid.length - 1][i].setVelocityX(10);
                    grid[grid.length - 1][i].setVelocityY(0);
                    grid[grid.length - 1][i].setPressure(0);
                }
                break;
        }
    }

    public String toString() {
        String resp = "";
        for (int i = grid.length - 1; i > -1; i--) {
            for (int j = 0; j < grid[i].length; j++) {
                resp += grid[i][j].toString() + "\t";
            }
            resp += "\n";
        }
        return resp;
    }
}
