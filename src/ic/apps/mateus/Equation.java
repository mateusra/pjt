package ic.apps.mateus;

public class Equation {

    private String funcao;

    public Equation(String funcao) {
        this.funcao = funcao;
    }

    public double calcula(double[] variaveis) {
        return this.resolve(funcao, variaveis);
    }

    private double resolve(String funcao, double[] variaveis) {
        boolean tira = true;
        System.out.println(funcao);
//        for (int i = 1; i < funcao.length() - 1; i++) {
//            if (funcao.charAt(i) == '(') {
//                tira = false;
//            } else if (funcao.charAt(i) == ')') {
//                tira = true;
//            }
//        }
//        if (funcao.indexOf("(") == 0 && funcao.lastIndexOf(")") == funcao.length() - 1 && tira) {
//            funcao = funcao.substring(1, funcao.length() - 1);
//        }
        char[] funcaoVet = funcao.toCharArray();
        int nivel = 0;
        char operador = ' ';
        int i;
        for (i = 0; i < funcaoVet.length; i++) {
            if (funcaoVet[i] == '(') {
                nivel++;
            } else if (funcaoVet[i] == ')') {
                nivel--;
            } else if (nivel == 0 && (funcaoVet[i] == '+' || (funcaoVet[i] == '-') ^ ((i - 1 >= 0) && funcaoVet[i - 1] == 'E'))) {
                operador = funcaoVet[i];
                break;
            }
        }
        if (operador == '+') {
            return resolve(funcao.substring(0, i), variaveis) + resolve(funcao.substring(i + 1), variaveis);
        } else if (operador == '-') {
            if (i == 0) {
                return -resolve(funcao.substring(i + 1), variaveis);
            }
            return resolve(funcao.substring(0, i), variaveis) - resolve(funcao.substring(i + 1), variaveis);
        }
        nivel = 0;
        operador = ' ';
        for (i = 0; i < funcaoVet.length; i++) {
            if (funcaoVet[i] == '(') {
                nivel++;
            } else if (funcaoVet[i] == ')') {
                nivel--;
            } else if (nivel == 0 && (funcaoVet[i] == '*' || funcaoVet[i] == '/')) {
                operador = funcaoVet[i];
                break;
            }
        }
        if (operador == '*') {
            return resolve(funcao.substring(0, i), variaveis) * resolve(funcao.substring(i + 1), variaveis);
        } else if (operador == '/') {
            return resolve(funcao.substring(0, i), variaveis) / resolve(funcao.substring(i + 1), variaveis);
        }
        nivel = 0;
        operador = ' ';
        for (i = 0; i < funcaoVet.length; i++) {
            if (funcaoVet[i] == '(') {
                nivel++;
            } else if (funcaoVet[i] == ')') {
                nivel--;
            } else if (nivel == 0 && (funcaoVet[i] == '^')) {
                operador = funcaoVet[i];
                break;
            } else if (nivel == 0 && (funcaoVet[i] == 'E')) {
                operador = funcaoVet[i];
                break;
            }
        }
        if (operador == '^') {
            return Math.pow(resolve(funcao.substring(0, i), variaveis), resolve(funcao.substring(i + 1), variaveis));
        } else if (operador == 'E') {
            return resolve(funcao.substring(0, i), variaveis) * Math.pow(10, resolve(funcao.substring(i + 1), variaveis));
        }
        if (funcao.charAt(0) == '(' && funcao.charAt(funcao.length() - 1) == ')') {
            return resolve(funcao.substring(1, funcao.length() - 1), variaveis);
        } else if (funcao.contains("x")) {
            return variaveis[Integer.parseInt(funcao.substring(1))];
        } else {
            return Double.parseDouble(funcao);
        }
    }
    
    public String toString(){
        return funcao;
    }
}
