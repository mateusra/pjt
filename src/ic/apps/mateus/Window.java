package ic.apps.mateus;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class Window {

    public Window(int l, int a) throws LWJGLException {
        Display.setDisplayMode(new DisplayMode(l, a));
        Display.setTitle("Aplicação");
        Display.sync(60);
        Display.create();
    }

    public boolean isAlive() {
        return !Display.isCloseRequested();
    }

    public void destroy() {
        Display.destroy();
    }

    public void update() {

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, Display.getWidth(), 0, Display.getHeight(), 1, -1);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);

        Display.update();

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(1, 1, 1, 1);

    }

}
