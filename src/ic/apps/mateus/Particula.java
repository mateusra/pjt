package ic.apps.mateus;

/**
 * Created by mateusra on 16/02/14.
 */
public class Particula {

    private double massa, volume, densidade, viscosidade, pressao;
    private Vetor posicao, velocidade;

    public double getPressao() {
        return pressao;
    }

    public void setPressao(double pressao) {
        this.pressao = pressao;
    }

    public double getViscosidade() {
        return viscosidade;
    }

    public void setViscosidade(double viscosidade) {
        this.viscosidade = viscosidade;
    }

    public Particula(Vetor posicao, double massa, double volume) {
        this.posicao = posicao;
        this.massa = massa;
        this.volume = volume;
        this.densidade = massa / volume;
        this.velocidade = new Vetor(0);
    }

    public double getMassa() {
        return massa;
    }

    public void setMassa(double massa) {
        this.massa = massa;
        this.densidade = massa / volume;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
        this.densidade = massa / volume;
    }

    public double getDensidade() {
        return densidade;
    }

    public Vetor getPosicao() {
        return posicao;
    }

    public void setPosicao(Vetor posicao) {
        this.posicao = posicao;
    }

    public Vetor getVelocidade() {
        return velocidade;
    }

    public void setVelocidade(Vetor velocidade) {
        this.velocidade = velocidade;
    }
}
