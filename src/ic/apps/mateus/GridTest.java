package ic.apps.mateus;

import ic.mlibs.structures.Grid;
import ic.mlibs.structures.GridCell;
import ic.mlibs.structures.Point3D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GridTest extends Grid {

    private Point3D max, min;
    private Equation[] equacoes = new Equation[3];
    private final double viscosidade = 0.001;

    public GridTest(double szC) {
        super(szC);
        this.criaFuncoes();
    }

    private void criaFuncoes() {
        /*variaveis
         x0 == U(i,j), x3 == U(i-1,j), x6 == U(i,j-1), x9 == U(i+1,j), x12 == U(i,j+1)
         x1 == V(i,j), x4 == V(i-1,j), x7 == V(i,j-1), x10 == V(i+1,j), x13 == V(i,j+1)
         x2 == P(i,j), x5 == P(i-1,j), x8 == P(i,j-1), x11 == P(i+1,j), x14 == P(i,j+1)*/

        double dt = 0.001;
        double ds = 0.1;

        double k0 = dt / ds, k1 = (dt * viscosidade) / (ds * ds);
        this.equacoes[0] = new Equation("x0-" + k0 + "*((x14-x8)/2+x0*(x0-x6)+x1*(x0-x3))+" + k1 + "*(x3+x6-4*x0+x9+x12)");
        this.equacoes[1] = new Equation("x1-" + k0 + "*((x11-x5)/2+x0*(x1-x7)+x1*(x1-x4))+" + k1 + "*(x4+x7-4*x1+x10+x13)");
        this.equacoes[2] = new Equation("(x14+x8+x5+x11)/4-((x12-x6+x10-x4)/" + dt + "-((x12-x6)^2+(x10-x4)^2+2*(x9-x3)*(x13-x7))/" + (2 * ds) + ")*" + ds + "/8");
    }

    public GridTest(Point3D min, Point3D max, double szC) {
        super(min, max, szC);
        this.max = max;
        this.min = min;
        this.criaFuncoes();
    }

    public void povoaGrid() {
        for (GridCell gridCell : grid) {
            Particula p = new Particula(new Vetor(gridCell.getPos().getPosX(), gridCell.getPos().getPosY()), 1, 1);
            p.setViscosidade(100);
            gridCell.addPart(p);
        }
    }

    @Override
    public void insertObject(Object o) {
        if (o instanceof Particula) {
            double x = Math.round(((Particula) o).getPosicao().getComponenteX() / sizeC) * sizeC;
            double y = Math.round(((Particula) o).getPosicao().getComponenteY() / sizeC) * sizeC;
            for (GridCell cell : this.getGrid()) {
                if (cell.getPos().getPosX() == x && cell.getPos().getPosY() == y) {
                    cell.addPart((Particula) o);
                    return;
                }
            }
        }
    }

    private void contorno() {
        int numLinhas = this.getColuna(0).size();
        GridCell[][] grid = new GridCell[numLinhas][];
        for (int i = numLinhas - 1; i >= 0; i--) {
            grid[numLinhas - 1 - i] = this.getLinha(i).toArray(new GridCell[0]);
        }

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (i == 0) {
                    grid[i][j].getParts().get(0).setPressao(0);
                    grid[i][j].getParts().get(0).getVelocidade().setComponenteX(1);
                    grid[i][j].getParts().get(0).getVelocidade().setComponenteY(0);
                }
                if (j == 0) {
                    grid[i][j].getParts().get(0).setPressao(grid[i][j + 1].getParts().get(0).getPressao());
                    grid[i][j].getParts().get(0).getVelocidade().setComponenteX(0);
                    grid[i][j].getParts().get(0).getVelocidade().setComponenteY(0);
                }

                if (i == grid.length - 1) {
                    grid[i][j].getParts().get(0).setPressao(grid[i - 1][j].getParts().get(0).getPressao());
                    grid[i][j].getParts().get(0).getVelocidade().setComponenteX(0);
                    grid[i][j].getParts().get(0).getVelocidade().setComponenteY(0);
                }
                if (j == grid[i].length - 1) {
                    grid[i][j].getParts().get(0).setPressao(0);
                    grid[i][j].getParts().get(0).getVelocidade().setComponenteX(0);
                    grid[i][j].getParts().get(0).getVelocidade().setComponenteY(0);
                }

            }

        }

        for (int i = 0; i < grid[0].length; i++) {
            grid[0][i].getParts().get(0).getVelocidade().setComponenteX(1);

        }
    }

    public void calcGrid() throws IOException {
        contorno();
        int numLinhas = this.getColuna(0).size();
        GridCell[][] grid = new GridCell[numLinhas][];
        for (int i = 0; i < numLinhas; i++) {
            grid[i] = this.getLinha(i).toArray(new GridCell[0]);
        }
        double[][][] gridCalc = new double[grid.length][grid[0].length][3];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                /*variaveis
                 x0 == U(i,j), x3 == U(i-1,j), x6 == U(i,j-1), x9 == U(i+1,j), x12 == U(i,j+1)
                 x1 == V(i,j), x4 == V(i-1,j), x7 == V(i,j-1), x10 == V(i+1,j), x13 == V(i,j+1)
                 x2 == P(i,j), x5 == P(i-1,j), x8 == P(i,j-1), x11 == P(i+1,j), x14 == P(i,j+1)*/
                double[] variaveis = new double[15];
                variaveis[0] = grid[i][j].getParts().get(0).getVelocidade().getComponenteX();
                variaveis[1] = grid[i][j].getParts().get(0).getVelocidade().getComponenteY();
                variaveis[2] = grid[i][j].getParts().get(0).getPressao();

                //No esquerda
                if (j - 1 >= 0) {
                    variaveis[6] = grid[i][j - 1].getParts().get(0).getVelocidade().getComponenteX();
                    variaveis[7] = grid[i][j - 1].getParts().get(0).getVelocidade().getComponenteY();
                    variaveis[8] = grid[i][j - 1].getParts().get(0).getPressao();
                } else {
                    variaveis[6] = 0;
                    variaveis[7] = 0;
                    variaveis[8] = 0;
                }

                //No acima
                if (i - 1 >= 0) {
                    variaveis[3] = grid[i - 1][j].getParts().get(0).getVelocidade().getComponenteX();
                    variaveis[4] = grid[i - 1][j].getParts().get(0).getVelocidade().getComponenteY();
                    variaveis[5] = grid[i - 1][j].getParts().get(0).getPressao();
                } else {
                    variaveis[3] = 0;
                    variaveis[4] = 0;
                    variaveis[5] = 0;
                }

                //No direita
                if (j + 1 < grid[i].length) {
                    variaveis[12] = grid[i][j + 1].getParts().get(0).getVelocidade().getComponenteX();
                    variaveis[13] = grid[i][j + 1].getParts().get(0).getVelocidade().getComponenteY();
                    variaveis[14] = grid[i][j + 1].getParts().get(0).getPressao();
                } else {
                    variaveis[12] = 0;
                    variaveis[13] = 0;
                    variaveis[14] = 0;
                }

                //No abaixo
                if (i + 1 < grid.length) {
                    variaveis[9] = grid[i + 1][j].getParts().get(0).getVelocidade().getComponenteX();
                    variaveis[10] = grid[i + 1][j].getParts().get(0).getVelocidade().getComponenteY();
                    variaveis[11] = grid[i + 1][j].getParts().get(0).getPressao();
                } else {
                    variaveis[9] = 0;
                    variaveis[10] = 0;
                    variaveis[11] = 0;
                }
                grid[i][j].getParts().get(0).setPressao((equacoes[2].calcula(variaveis)));
                contorno();
            }
        }

//        for (int i = 0; i < gridCalc.length-1; i++) {
//            for (int j = 0; j < gridCalc[i].length; j++) {
//                grid[i][j].getParts().get(0).setPressao(gridCalc[i][j][2]);
//            }
//        }
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                /*variaveis
                 x0 == U(i,j), x3 == U(i-1,j), x6 == U(i,j-1), x9 == U(i+1,j), x12 == U(i,j+1)
                 x1 == V(i,j), x4 == V(i-1,j), x7 == V(i,j-1), x10 == V(i+1,j), x13 == V(i,j+1)
                 x2 == P(i,j), x5 == P(i-1,j), x8 == P(i,j-1), x11 == P(i+1,j), x14 == P(i,j+1)*/
                double[] variaveis = new double[15];
                variaveis[0] = grid[i][j].getParts().get(0).getVelocidade().getComponenteX();
                variaveis[1] = grid[i][j].getParts().get(0).getVelocidade().getComponenteY();
                variaveis[2] = grid[i][j].getParts().get(0).getPressao();

                //No esquerda
                if (j - 1 >= 0) {
                    variaveis[6] = grid[i][j - 1].getParts().get(0).getVelocidade().getComponenteX();
                    variaveis[7] = grid[i][j - 1].getParts().get(0).getVelocidade().getComponenteY();
                    variaveis[8] = grid[i][j - 1].getParts().get(0).getPressao();
                } else {
                    variaveis[6] = 0;
                    variaveis[7] = 0;
                    variaveis[8] = 0;
                }

                //No acima
                if (i - 1 >= 0) {
                    variaveis[3] = grid[i - 1][j].getParts().get(0).getVelocidade().getComponenteX();
                    variaveis[4] = grid[i - 1][j].getParts().get(0).getVelocidade().getComponenteY();
                    variaveis[5] = grid[i - 1][j].getParts().get(0).getPressao();
                } else {
                    variaveis[3] = 0;
                    variaveis[4] = 0;
                    variaveis[5] = 0;
                }

                //No direita
                if (j + 1 < grid[i].length) {
                    variaveis[12] = grid[i][j + 1].getParts().get(0).getVelocidade().getComponenteX();
                    variaveis[13] = grid[i][j + 1].getParts().get(0).getVelocidade().getComponenteY();
                    variaveis[14] = grid[i][j + 1].getParts().get(0).getPressao();
                } else {
                    variaveis[12] = 0;
                    variaveis[13] = 0;
                    variaveis[14] = 0;
                }

                //No abaixo
                if (i + 1 < grid.length) {
                    variaveis[9] = grid[i + 1][j].getParts().get(0).getVelocidade().getComponenteX();
                    variaveis[10] = grid[i + 1][j].getParts().get(0).getVelocidade().getComponenteY();
                    variaveis[11] = grid[i + 1][j].getParts().get(0).getPressao();
                } else {
                    variaveis[9] = 0;
                    variaveis[10] = 0;
                    variaveis[11] = 0;
                }
                grid[i][j].getParts().get(0).getVelocidade().setComponenteX(equacoes[0].calcula(variaveis));
                grid[i][j].getParts().get(0).getVelocidade().setComponenteY(equacoes[1].calcula(variaveis));
                contorno();
            }
        }
//        for (int i = 0; i < gridCalc.length-1; i++) {
//            for (int j = 0; j < gridCalc[i].length; j++) {
//                grid[i][j].getParts().get(0).getVelocidade().setComponenteX(gridCalc[i][j][0]);
//                grid[i][j].getParts().get(0).getVelocidade().setComponenteY(gridCalc[i][j][1]);
//            }
//        }
//
//        for (int i = 1; i < grid.length; i++) {
//            for (int j = 0; j < grid[i].length; j++) {
//                /*variaveis
//                 x0 == U(i,j), x3 == U(i-1,j), x6 == U(i,j-1), x9 == U(i+1,j), x12 == U(i,j+1)
//                 x1 == V(i,j), x4 == V(i-1,j), x7 == V(i,j-1), x10 == V(i+1,j), x13 == V(i,j+1)
//                 x2 == P(i,j), x5 == P(i-1,j), x8 == P(i,j-1), x11 == P(i+1,j), x14 == P(i,j+1)*/
//                double[] variaveis = new double[15];
//                variaveis[0] = grid[i][j].getParts().get(0).getVelocidade().getComponenteX();
//                variaveis[1] = grid[i][j].getParts().get(0).getVelocidade().getComponenteY();
//                variaveis[2] = grid[i][j].getParts().get(0).getPressao();
//
//                //No esquerda
//                if (j - 1 >= 0) {
//                    variaveis[6] = grid[i][j - 1].getParts().get(0).getVelocidade().getComponenteX();
//                    variaveis[7] = grid[i][j - 1].getParts().get(0).getVelocidade().getComponenteY();
//                    variaveis[8] = grid[i][j - 1].getParts().get(0).getPressao();
//                } else {
//                    variaveis[6] = 0;
//                    variaveis[7] = 0;
//                    variaveis[8] = 0;
//                }
//
//                //No acima
//                if (i - 1 >= 0) {
//                    variaveis[3] = grid[i - 1][j].getParts().get(0).getVelocidade().getComponenteX();
//                    variaveis[4] = grid[i - 1][j].getParts().get(0).getVelocidade().getComponenteY();
//                    variaveis[5] = grid[i - 1][j].getParts().get(0).getPressao();
//                } else {
//                    variaveis[3] = 0;
//                    variaveis[4] = 0;
//                    variaveis[5] = 0;
//                }
//
//                //No direita
//                if (j + 1 < grid[i].length) {
//                    variaveis[12] = grid[i][j + 1].getParts().get(0).getVelocidade().getComponenteX();
//                    variaveis[13] = grid[i][j + 1].getParts().get(0).getVelocidade().getComponenteY();
//                    variaveis[14] = grid[i][j + 1].getParts().get(0).getPressao();
//                } else {
//                    variaveis[12] = 0;
//                    variaveis[13] = 0;
//                    variaveis[14] = 0;
//                }
//
//                //No abaixo
//                if (i + 1 < grid.length) {
//                    variaveis[9] = grid[i + 1][j].getParts().get(0).getVelocidade().getComponenteX();
//                    variaveis[10] = grid[i + 1][j].getParts().get(0).getVelocidade().getComponenteY();
//                    variaveis[11] = grid[i + 1][j].getParts().get(0).getPressao();
//                } else {
//                    variaveis[9] = 0;
//                    variaveis[10] = 0;
//                    variaveis[11] = 0;
//                }
//                gridCalc[i][j][2] = (equacoes[2].calcula(variaveis));
//            }
//        }
//
//        for (int i = 1; i < gridCalc.length; i++) {
//            for (int j = 0; j < gridCalc[i].length; j++) {
//                grid[i][j].getParts().get(0).setPressao(gridCalc[i][j][2]);
//            }
//        }
        //this.contorno();

    }

    public void calculaPressao() throws IOException {
        int numLinhas = this.getColuna(0).size();
        int numColunas = this.getLinha(0).size();
        double[][] sistema = new double[numColunas * numLinhas][numColunas * numLinhas];
        for (int i = 0; i < sistema.length; i++) {

            //Preenchendo as linhas como matrizes pra depois tranhsformar pra ficar mais facil
            double[][] grid = new double[numLinhas][numColunas];
            int j = i / numColunas;
            int k = i % numColunas;
            grid[j][k] = -4;

            //Estencil de 5 pontos 
            if (k + 1 < numColunas) {
                grid[j][k + 1] = 1;
            }
            if (k - 1 >= 0) {
                grid[j][k - 1] = 1;
            }
            if (j + 1 < numLinhas) {
                grid[j + 1][k] = 1;
            }
            if (j - 1 >= 0) {
                grid[j - 1][k] = 1;
            }

            //transformando a matriz para uma linha
            sistema[i] = Help.matrizParaLinha(grid);
        }
        Help.escreveMatriz(Help.linhaParaMatriz(sistema[5], numLinhas), "Linha como Matriz.txt");
        Help.escreveMatriz(sistema, "Sistema - Completo.txt");

        //vetor constante
        double[] coeficentes = new double[numColunas * numLinhas];
        for (int i = 0; i < coeficentes.length; i++) {
            coeficentes[i] = 0;
        }

        //colocando valores na linha mais abaixo do grid
        for (int i = coeficentes.length - 1; i >= coeficentes.length - numColunas; i--) {
            coeficentes[i] = 100;
        }

        //transformando as linhas correspondentes aos valores acima para a identidade
        for (int i = sistema.length - 1; i >= sistema.length - numColunas; i--) {
            sistema[i] = new double[numColunas * numLinhas];
            sistema[i][i] = 1;
        }

        //colocando valores na linha mais acima do grid
        for (int i = 0; i < numColunas; i++) {
            coeficentes[i] = 0;
        }

        //transformando as linhas correspondentes aos valores acima para a identidade
        for (int i = 0; i < numColunas; i++) {
            sistema[i] = new double[numColunas * numLinhas];
            sistema[i][i] = 1;
        }

        Help.escreveVetor(coeficentes, "Vetor Constante.txt");

        //transformando a solucao pra matriz de novo...
        double[][] pressao = Help.linhaParaMatriz(Help.Gauss(sistema, coeficentes), numLinhas);

        Help.escreveMatriz(pressao, "Matriz de Pressao.txt");

        for (int i = 0; i < pressao.length; i++) {
            List<GridCell> linha = this.getLinha(pressao.length - 1 - i);
            for (int j = 0; j < pressao[i].length; j++) {
                linha.get(j).getParts().get(0).setPressao(pressao[i][j]);
            }
        }
    }

    public List<GridCell> getLinha(int num) {
        List<GridCell> linha = new ArrayList<>();
        for (GridCell cell : this.getGrid()) {
            if ((cell.getPos().getPosY() - min.getPosY()) == num * sizeC && cell.getPos().getPosX() <= max.getPosX() && cell.getPos().getPosZ() == 0) {
                linha.add(cell);
            }
        }
        return linha;
    }

    public List<GridCell> getColuna(int num) {
        List<GridCell> coluna = new ArrayList<>();
        for (GridCell cell : this.getGrid()) {
            if ((cell.getPos().getPosX() - min.getPosX()) == num * sizeC && cell.getPos().getPosY() <= max.getPosY() && cell.getPos().getPosZ() == 0) {
                coluna.add(cell);
            }
        }
        return coluna;
    }
}
