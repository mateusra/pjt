package ic.apps.mateus;

public class Cell {

    private double x, y, u, v, p;

    public Cell(double x, double y) {
        this.x = x;
        this.y = y;
        this.u = 0;
        this.v = 0;
        this.p = 0;
    }

    public double getPositionX() {
        return this.x;
    }

    public double getPositionY() {
        return this.y;
    }

    public double getVelocityX() {
        return this.u;
    }

    public double getVelocityY() {
        return this.v;
    }

    public double getPressure() {
        return this.p;
    }

    public double getVelocityModule() {
        return Math.sqrt(u * u + v * v);
    }

    public void setPositionX(double x) {
        this.x = x;
    }

    public void setPositionY(double y) {
        this.y = y;
    }

    public void setVelocityX(double u) {
        this.u = u;
    }

    public void setVelocityY(double v) {
        this.v = v;
    }

    public void setPressure(double p) {
        this.p = p;
    }
    
    public Cell clone(){
        Cell c = new Cell(x, y);
        c.setVelocityX(u);
        c.setVelocityY(v);
        c.setPressure(p);
        return c;
    }

    public String toString() {
        return String.format("(%.2f, %.2f, %.2f) ", this.u, this.v, this.p);
    }
}
