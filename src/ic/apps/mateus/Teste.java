package ic.apps.mateus;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class Teste {

    static int modo = 1;
    static DefaultXYDataset veloX, veloY, pressao;
    static JFreeChart veloXC, veloYC, pressaoC;
    static ChartPanel veloXP, veloYP, pressaoP;
    static JFrame frame;
    static JPanel painel;
    /* modo == 1 --> mapa da velocidade
     modo == 2 --> mapa da velocidade + vetores
     modo == 3 --> mapa da pressao
    
     simulacao == 1 --> simulacao original
     simulacao == 2 --> simulacao original com velocidade na tampa  == 10
     simulacao == 3 --> simulacao de circulo na caixa com velocidade == 10
     simulacao == 4 --> simulacao com velocidade == 10 nas linhas superior e inferior
     simulacao == 5 --> simulacao com velocidade == 10 de colisao nas arestas (0,0) e (xMax,yMax);
     */

    public static void main(String[] args) throws Exception {
        double ds = 5;
        Grid g = new Grid(new Cell(0, 0), new Cell(800, 600), ds);
        Cell[][] grid = g.getGrid();
        g.setTime(0.001);
        g.setRho(1);
        g.setViscosity(0.044);
        g.setDx(0.05);
        g.setDy(0.05);
        int simulacao = 1;
        g.setBoundary(simulacao);
        Window w = new Window(800, 600);
        Mouse.create();
        Keyboard.create();
        int cont = 0;
        int step = 20;
        makeCharts();
        while (w.isAlive()) {
            cont++;
            if (Keyboard.isKeyDown(Keyboard.KEY_1)) {
                modo = 1;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_2)) {
                modo = 2;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_3)) {
                modo = 3;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_4)) {
                modo = 4;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_R)) {
                g.startCond();
            } else if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD0)) {
                simulacao = 0;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD1)) {
                simulacao = 1;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD2)) {
                simulacao = 2;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD3)) {
                simulacao = 3;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD4)) {
                simulacao = 4;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD5)) {
                simulacao = 5;
            } else if (Keyboard.isKeyDown(Keyboard.KEY_C)) {
                frame.setVisible(true);
            } else if (Keyboard.isKeyDown(Keyboard.KEY_V)) {
                frame.setVisible(false);
            } else if (Keyboard.isKeyDown(Keyboard.KEY_B)) {    
                upadeCharts(g);
            }
            if (Mouse.isButtonDown(0)) {
                

                int c = (int) (Mouse.getX() / ds);
                int l = (int) (Mouse.getY() / ds);
                double vMax = (simulacao==1)?1:10;
                grid[l][c].setVelocityX(0);
                grid[l][c].setVelocityY(0);
                grid[l][c].setPressure(1);
                if (c + 1 < grid[l].length) {
                    grid[l][c + 1].setVelocityX(vMax);
                    grid[l][c + 1].setVelocityY(0);
                    grid[l][c + 1].setPressure(1);
                }

                if (c - 1 >= 0) {
                    grid[l][c - 1].setVelocityX(-vMax);
                    grid[l][c - 1].setVelocityY(0);
                    grid[l][c - 1].setPressure(1);
                }
                if (l + 1 < grid.length) {
                    grid[l + 1][c].setVelocityX(0);
                    grid[l + 1][c].setVelocityY(vMax);
                    grid[l + 1][c].setPressure(1);
                }
                if (l - 1 >= 0) {
                    grid[l - 1][c].setVelocityX(0);
                    grid[l - 1][c].setVelocityY(-vMax);
                    grid[l - 1][c].setPressure(1);
                }
            }

            for (int i = 0; i < step; i++) {
                g.cavityFlow(simulacao);
            }
            gridColor(grid);
            w.update();
        }

        w.destroy();

        System.out.println("Número de iterações: " + cont * step);
        frame.setVisible(true);
        upadeCharts(g);
    }

    public static void makeCharts() {
        frame = new JFrame("Gráficos");
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        veloX = new DefaultXYDataset();
        veloXC = ChartFactory.createXYLineChart("Velocidade U x Y", "U", "Y", veloX, PlotOrientation.VERTICAL, false, false, false);
        veloXP = new ChartPanel(veloXC);
        veloXP.setPreferredSize(new Dimension(400, 300));

        veloY = new DefaultXYDataset();
        veloYC = ChartFactory.createXYLineChart("Velocidade U x X", "X", "U", veloY, PlotOrientation.VERTICAL, false, false, false);
        veloYP = new ChartPanel(veloYC);
        veloYP.setPreferredSize(new Dimension(400, 300));
        
        frame.add(veloXP);
        frame.add(veloYP);
        frame.pack();
    }

    public static void upadeCharts(Grid grid) {
        Cell[][] g = grid.getGrid();
        double ds = grid.getDy();
        double[] posicao = new double[g.length];
        for (int i = 1; i < posicao.length; i++) {
            posicao[i] = ds + posicao[i - 1];
        }

        double[][] dataVeloX = new double[2][posicao.length];
        dataVeloX[1] = posicao;

        for (int i = 0; i < g.length; i++) {
            dataVeloX[0][i] = g[i][g.length / 2].getVelocityX();
        }
        veloX.removeSeries("U x Y");
        veloX.addSeries("U x Y", dataVeloX);
        posicao = new double[g[g.length / 2].length];
        for (int i = 1; i < posicao.length; i++) {
            posicao[i] = ds + posicao[i - 1];
        }
        double[][] dataVeloY = new double[2][posicao.length];
        dataVeloY[0] = posicao;

        for (int i = 0; i < g[g.length / 2].length; i++) {
            dataVeloY[1][i] = g[g.length / 2][i].getVelocityY();
        }
        veloY.removeSeries("U x X");
        veloY.addSeries("U x X", dataVeloY);
    }

    public static void drawVector(Cell c, double lado, double maiorVeloX, double maiorVeloY) {
        double xMax = c.getPositionX() + (c.getVelocityX() / maiorVeloX) * lado;
        double yMax = c.getPositionY() + (c.getVelocityY() / maiorVeloY) * lado;
            GL11.glLineWidth(3);
            GL11.glBegin(GL11.GL_LINES);
            GL11.glColor3d(0.9, 0, 0);
            GL11.glVertex2d(c.getPositionX(), c.getPositionY());
            GL11.glVertex2d(xMax, yMax);
            GL11.glEnd();

            GL11.glBegin(GL11.GL_TRIANGLES);

            GL11.glColor3d(0.9, 0, 0);
            GL11.glVertex2d(xMax + 10 * (c.getVelocityX() / c.getVelocityModule()), yMax + 10 * (c.getVelocityY() / c.getVelocityModule()));
            GL11.glVertex2d(xMax + 5 * (c.getVelocityY() / c.getVelocityModule()), yMax - 5 * (c.getVelocityX() / c.getVelocityModule()));
            GL11.glVertex2d(xMax - 5 * (c.getVelocityY() / c.getVelocityModule()), yMax + 5 * (c.getVelocityX() / c.getVelocityModule()));
            GL11.glEnd();
        
    }

    public static void drawCell(Cell c, double maxVelo, double maxPress) {
        double pres = ((c.getPressure() != 0) ? c.getPressure() / Math.abs(c.getPressure()) : 1) * (maxPress - Math.abs(c.getPressure())) / maxPress;
        double velo = (c.getVelocityModule() / maxVelo) * 0.6;
        if (modo != 3) {
            GL11.glColor3d(0.1 + velo * 0.2, 0.15 + velo * 0.2, 0.3 + velo);
        } else {
            GL11.glColor3d(pres, Math.abs(pres * 0.2), -pres);
        }
        GL11.glVertex2d(c.getPositionX(), c.getPositionY());
    }

    public static void gridColor(Cell[][] g) {
        double maiorPressao = Math.abs(g[0][0].getPressure());
        double maiorVeloX = Math.abs(g[0][0].getVelocityX());
        double maiorVeloY = Math.abs(g[0][0].getVelocityY());
        for (Cell[] cells : g) {
            for (Cell cell : cells) {
                if (Math.abs(cell.getPressure()) > maiorPressao) {
                    maiorPressao = Math.abs(cell.getPressure());
                }
                if (Math.abs(cell.getVelocityX()) > maiorVeloX) {
                    maiorVeloX = Math.abs(cell.getVelocityX());
                }
                if (Math.abs(cell.getVelocityY()) > maiorVeloY) {
                    maiorVeloY = Math.abs(cell.getVelocityY());
                }
            }
        }
        for (int i = 1; i < g.length; i++) {
            for (int j = 1; j < g[i].length; j++) {
                GL11.glBegin(GL11.GL_TRIANGLES);
                drawCell(g[i][j - 1], Math.sqrt(maiorVeloX * maiorVeloX + maiorVeloY * maiorVeloY), maiorPressao);
                drawCell(g[i - 1][j - 1], Math.sqrt(maiorVeloX * maiorVeloX + maiorVeloY * maiorVeloY), maiorPressao);
                drawCell(g[i - 1][j], Math.sqrt(maiorVeloX * maiorVeloX + maiorVeloY * maiorVeloY), maiorPressao);
                GL11.glEnd();

                GL11.glBegin(GL11.GL_TRIANGLES);
                drawCell(g[i][j - 1], Math.sqrt(maiorVeloX * maiorVeloX + maiorVeloY * maiorVeloY), maiorPressao);
                drawCell(g[i][j], Math.sqrt(maiorVeloX * maiorVeloX + maiorVeloY * maiorVeloY), maiorPressao);
                drawCell(g[i - 1][j], Math.sqrt(maiorVeloX * maiorVeloX + maiorVeloY * maiorVeloY), maiorPressao);
                GL11.glEnd();
            }
        }

        if (modo == 2) {
            for (Cell[] cells : g) {
                for (Cell c : cells) {
                    if (c.getPositionX() % 50 == 0 && c.getPositionY() % 50 == 0 && c.getVelocityModule() > 0.1) {
                        drawVector(c, 50, maiorVeloX, maiorVeloY);
                    }
                }
            }
        }
    }
}
