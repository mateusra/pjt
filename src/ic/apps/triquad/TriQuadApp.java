
package ic.apps.triquad;

import ic.mlibs.util.Application;

/**
 *
 * @author marcoslage
 */
public class TriQuadApp extends TriQuadSettings implements Application{

    private TriQuadStackGLMesh  quadbuilder; // modelo 3d de trig
    private TriQuadAdaptGLMesh  quadmultirs; // modelo 3d de quad em multiresolution
    
    private static boolean runned = Boolean.FALSE;
  
    public TriQuadApp(String confFile){
        // Construtor de DopsSettings
        super(confFile);
        // GLMesh allocation
        quadbuilder  = new TriQuadStackGLMesh();
        quadmultirs  = null;
    }
    
    @Override
    public void create() throws Exception {
        try {
            quadbuilder.readChe( getMfile() );
        }
        catch (Exception e) {
            //Destroi a aplicação
            destroy();
            //Log
            System.out.println("Erro em ExApp::create");
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        // Destroy a malha
        quadbuilder = null;
        quadmultirs = null;
        // Limpa as configurações
        clearSettings();
    }

    @Override
    public void run() {
        
        if(!runned){
            //executa
            quadmultirs = quadbuilder.run();
            runned = Boolean.TRUE;        
        }
    }

    @Override
    public void render() {
        if( runned ){
            quadmultirs.render( getCmap(), getRmode(), getCmapMin(), getCmapMax() );
        }
        else
            quadbuilder.render( getCmap(), getRmode(), getCmapMin(), getCmapMax() );
    }

    @Override
    public void reset() {
        //releitura das configurações
        loadSettings();
        // GLMesh allocation
        quadbuilder = new TriQuadStackGLMesh();
    }

    @Override
    public void reload() {
        //releitura das configurações
        loadSettings();
    }
    
}
