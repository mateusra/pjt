
package ic.apps.triquad;

import ic.mlibs.gl.GLMesh;
import ic.mlibs.structures.Point3D;
import ic.mlibs.util.Helper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.PriorityQueue;

/**
 *
 * @author marcoslage
 */
public class TriQuadStackGLMesh extends GLMesh {
    
    //----------------------------------------------------------------------
    public class EdgeProps implements Comparable{
        public double weight;
        public int    edgeid;

        
        public EdgeProps(int he, double we){
            this.edgeid = he;
            this.weight = we;
        }
        
        @Override
        public int compareTo(Object t) {
            EdgeProps tmp = (EdgeProps) t;
            
            if(this.weight > tmp.weight) 
                return  1;
            else if(this.weight < tmp.weight) 
                return -1;
            else
                return  0;
        }
     }
    //-----------------------------------------------------------------------
    
    private static boolean runned = Boolean.FALSE;
    private PriorityQueue<EdgeProps> pqueue = new PriorityQueue<EdgeProps>();
    
    TriQuadAdaptGLMesh run() {
        if(!runned){
            checkPairs();
            debugMesh();
                        
            runned = true;

            return matchTrigs();
        }
        
        return null;
    }

    private int getEdge(int he) {
        
        int ohe = this.getO(he);
        int edge = Math.min(he, ohe);
        
        return (ohe != -1)?(edge):(he);
    }
    
    // checks if the mesh has even triangles.
    private boolean checkPairs() {
        
        if( this.getNquad() != 0 ) return false;
        
        if( this.getNtrig() % 2 == 0 ) return true;
        else 
            if( this.getNcurv() > 0 ) {
                
                //Retorna os pontos no bordo.
                int bdhe = this.getB(0);
                Point3D ibdPoint = this.getG( this.getV(bdhe) ) ;
                
                int nthe = this.next(bdhe);
                Point3D fbdPoint = this.getG( this.getV(nthe) ) ;
                
                //Calcula o ponto médio.
                Point3D newPoint = Helper.midpoint(ibdPoint, fbdPoint);
                newPoint.setObj(2.0);
                
                //adiciona a malha.
                this.fixPairs( newPoint, bdhe );
                
                return true;
            }
            else 
                return false; 
    }
    
    // fix meshes with odd number of triangle.
    private void fixPairs(Point3D newPoint, int he) {

        if( this.getNquad() != 0 ) return ;
        
            System.out.println("TriQuadGLMesh::fixPairs called!");
        
            //resize do vetor
            this.G. ensureCapacity(   this.nvert+1  );
            //resize do vetor
            this.VH.ensureCapacity(   this.nvert+1  );
            //resize do vetor
            this.VT.ensureCapacity(3*(this.ntrig+1));
            //resize do vetor
            this.OT.ensureCapacity(3*(this.ntrig+1));
            
            //------G
            
            //adds the point
            this.G.add(newPoint);
            this.nvert++;

            //------VT

            //constroi novo trig
            int v0 = this.getNvert()-1;
            int v1 = this.getV(this.next(he));
            int v2 = this.getV(this.prev(he));
            
            //update do vértice do trig antigo.
            this.setV(v0, this.next(he));
            
            //criação do novo trig
            this.VT.add(v0); this.VT.add(v1); this.VT.add(v2);
            //update ntrig
            this.ntrig++;
             
            //------OT
          
            //update da tabela O
            int he0 = 3*(this.ntrig-1);
            int he1 = he0+1;
            int he2 = he1+1;
            
            int oldO = this.getO(this.next(he));
            
            //increases opsite table
            this.OT.add(-1); this.OT.add(-1); this.OT.add(-1);

            //update he central
            this.setO(he2,this.next(he));
            this.setO(this.next(he),he2);
            
            //update da next
            this.setO(oldO,he1);
            if(oldO>0) this.setO(he1,oldO);
            
            //------VH
            
            //atualiza a VH
            this.VH.add(v0);
            if(oldO == -1) this.setVH(he1,v1);
            
            //------EH
            
            //corrige a antiga
            this.EH.put( this.base( this.getNtrig()-1 ), -1);
            
            //corrige a antiga
            int min = Math.min(he2, this.next(he));
            int max = Math.max(he2, this.next(he));
            this.EH.put(min,max);
            
            //adiciona central
            if(oldO == -1) { min = he1; max = -1; }
            else{ min = Math.min(he1, oldO); max = Math.max(he1, oldO); }
            this.EH.put(min,max);
}

    // 
    private void buildPriority() {
        
        Iterator<Integer> i = this.EH.keySet().iterator();
        while(i.hasNext()){
            
            int  he = i.next();
            this.setEH(he, Boolean.FALSE);
            
            if(this.getO(he) == -1) {
                this.setEH(he, Boolean.TRUE);
                continue;
            }
            
            Point3D v0 = this.getG( this.getV(he) );
            he = this.prev(he);
            Point3D v1 = this.getG( this.getV(he) );
            he = this.getO( this.next(he) );
            Point3D v2 = this.getG( this.getV(he) );
            he = this.prev(he);
            Point3D v3 = this.getG( this.getV(he) );
            
            double weight = rank(v0, v1, v2, v3);
            
            // creates the Queue
            EdgeProps ep = new EdgeProps(he,weight);
            // adds the edge
            pqueue.add(ep);            
        }
    }
   
    private static double rankEdgeLenght(Point3D v0, Point3D v1) {

        double [] vs  = new double[3];
        double lenght = 0.f;

        for (int k = 0; k < 3; ++k) {
            vs[k] = v1.getPos().getVecData(k) - v0.getPos().getVecData(k);
            lenght += (vs[k] * vs[k]);
        }

        return Math.sqrt( lenght );

    }
    
    private static double rankSquareness(Point3D v0, Point3D v1, Point3D v2, Point3D v3) {

         double vs01[] = new double[3];
         double vs02[] = new double[3];
         double vs31[] = new double[3];
         double vs32[] = new double[3];
         
         double l_01 = 0.f, 
                l_02 = 0.f, 
                l_31 = 0.f, 
                l_32 = 0.f, 
             dot_012 = 0.f,
             dot_312 = 0.f;

        for (int k = 0; k < 3; ++k) {
            vs01[k] = v1.getPos().getVecData(k) - v0.getPos().getVecData(k);
            vs02[k] = v2.getPos().getVecData(k) - v0.getPos().getVecData(k);
            vs31[k] = v1.getPos().getVecData(k) - v3.getPos().getVecData(k);
            vs32[k] = v2.getPos().getVecData(k) - v3.getPos().getVecData(k);
            
            l_01 += (vs01[k] * vs01[k]);
            l_02 += (vs02[k] * vs02[k]);
            l_31 += (vs31[k] * vs31[k]);
            l_32 += (vs32[k] * vs32[k]);
        }

        l_01 = Math.sqrt( l_01 );
        l_02 = Math.sqrt( l_02 );
        l_31 = Math.sqrt( l_31 );
        l_32 = Math.sqrt( l_32 );

        for (int k = 0; k < 3; ++k) {
            dot_012 += (vs01[k] / l_01) * (vs02[k] / l_02);
            dot_312 += (vs31[k] / l_31) * (vs32[k] / l_32);
        }

        return (Math.abs(dot_012) + Math.abs(dot_312));
    }
    
    private static double rank(Point3D v0, Point3D v1, Point3D v2, Point3D v3){
        
        return rankEdgeLenght(v0, v2) + rankSquareness(v0, v1, v2, v3);
        
    }
       
    private TriQuadAdaptGLMesh matchTrigs() {
        //creates the priority queue
        buildPriority();
        
        // allocs the new VQ array.
        ArrayList<Integer> newVQ = new ArrayList<Integer>();
        newVQ.ensureCapacity(ntrig / 2);
       
         while( !pqueue.isEmpty() ) {

            EdgeProps rh = pqueue.poll();

            if( !(Boolean)this.getEH(rh.edgeid) ) {
                
                int he   = getEdge( rh.edgeid );
                this.setEH(  he, Boolean.TRUE);
                
                int phe  = getEdge( this.prev( he ) );
                this.setEH( phe, Boolean.TRUE);

                int ohe  = getEdge( this.getO( he ) );
                this.setEH( ohe, Boolean.TRUE);

                int pohe = getEdge( this.prev(ohe ) );
                this.setEH(pohe, Boolean.TRUE);
                
                newVQ.add(this.getV(  he));
                newVQ.add(this.getV( phe));
                newVQ.add(this.getV( ohe));
                newVQ.add(this.getV(pohe));
            }
            
        }
        
        return new TriQuadAdaptGLMesh(this.G, newVQ);
    }
    
    
    
    private void debugMesh() {
        //V table
        for(int i=0; i<this.getNhe(); i++)
            System.out.println("V["+i+"]="+this.getV(i));
        System.out.println();
        
        //O table
        for(int i=0; i<this.getNhe(); i++)
            System.out.println("O["+i+"]="+this.getO(i));
        System.out.println();

        //VH table
        for(int i=0; i<this.getNvert(); i++)
            System.out.println("VH["+i+"]="+this.getVH(i));
        System.out.println();

        //VH table
        for(int i=0; i<this.getNedge(); i++)
            System.out.println("EH["+i+"]="+this.getEH(i));
        System.out.println();
    }
}
