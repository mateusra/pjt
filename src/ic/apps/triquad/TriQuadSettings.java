/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2013.1
 * TriQuad Settings
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.apps.triquad;

//Meus imports
import ic.mlibs.util.Settings;

public class TriQuadSettings extends Settings{
    private String mfile;  // modelo 3D
    private String rmode;  // modo de render
    private String  cmap;  // mapa de cores
    
    private double cmapMin; // menor valor do mapa de cores
    private double cmapMax; // maior valor do mapa de cores

    //--- Construtor

    public TriQuadSettings(String confsFile){
        super(confsFile);
    }

    //--- Acessores

    public String getMfile() {
        return mfile;
    }

    public String getRmode() {
        return rmode;
    }
    
    public String getCmap()  {
        return cmap;
    }

    public double getCmapMin() {
        return cmapMin;
    }
 
    public double getCmapMax() {
        return cmapMax;
    }

    //--- Atribuidores

     public void setCmapMax(double cmapMax) {
        this.cmapMax = cmapMax;
    }

    public void setCmapMin(double cmapMin) {
        this.cmapMin = cmapMin;
    }
    
    //--- Reimplementações
    
    @Override
    protected void clearSettings() {
         // Arquivo do modelo 3D
        mfile   = null;

        // Escolha do rendering
        rmode   = null;
        
        // Escolha do mapa de cores
        cmap    = null;
        
        // Mínimo da propriedade
        cmapMin = 0.0f;
        
        // Mínimo da propriedade
        cmapMax = 0.0f;
        
        // Superclass
        super.clearSettings();
    }
    
    //--- Implementações
    
    @Override
    protected void loadStrings() {
        // Arquivo do modelo 3D
        mfile   = settings.getProperty( "mfile" );

        // Escolha do rendering
        rmode   = settings.getProperty( "rmode" );
        
        // Escolha do mapa de cores
        cmap    = settings.getProperty( "cmap"  );
       
        // Mínimo da propriedade
        cmapMin = Double.parseDouble( settings.getProperty( "cmapMin" ) );
        
        // Mínimo da propriedade
        cmapMax = Double.parseDouble( settings.getProperty( "cmapMax" ) );
    }

    @Override
    protected void saveStrings() {
        // Arquivo do modelo 3D
        settings.setProperty("mfile" , mfile);

        // Escolha do rendering
        settings.setProperty( "rmode", rmode);
        
        // Escolha do mapa de cores
        settings.setProperty( "cmap" , cmap );
                
        // Escolha do valor minimo do mapa de cores
        settings.setProperty("cmapMin", Double.toString( cmapMin ) ); 
        
        // Escolha do valor minimo do mapa de cores
        settings.setProperty("cmapMax", Double.toString( cmapMax ) ); 
    }
}
