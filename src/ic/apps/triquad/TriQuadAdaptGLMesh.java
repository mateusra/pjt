
package ic.apps.triquad;

import ic.mlibs.gl.GLMesh;
import ic.mlibs.structures.Point3D;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author marcoslage
 */
class TriQuadAdaptGLMesh extends GLMesh {
    
    public TriQuadAdaptGLMesh( ArrayList<Point3D> G, ArrayList<Integer> VQ ){
        super();
        
        this.nvert = G.size();
        this.ntrig = 0;
        this.nquad = VQ.size() / 4;
        
        this.allocChe();
        
        //Copia o dado lido
        Collections.copy( this.G,   G );
        Collections.copy( this.VQ, VQ );

        // Cria a estrutura Che da malha lida
        this.build();
    }
}
