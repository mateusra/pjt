/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe GuiApp
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.apps.gui;

//Meus imports
import ic.apps.triquad.TriQuadApp;
import ic.mlibs.util.Application;

//LWJGL
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

//LWJGL estaticos
import static org.lwjgl.opengl.GL11.*;

//Java
import java.nio.DoubleBuffer;
import javax.swing.JFrame;

public class GuiApp extends GuiSettings implements Application {
   
    // Editor  
    private GuiEditor myEditor = null; //> editor do arquivo de configurações
    // Applicação
    private Application myApp  = null; //> aplicação que será executada
    // Booleano
    private boolean canRun     = false;//> indica se a aplicação está pronta para rodar

    public GuiApp( String confFile ) {
        // carrega as configs de interface
        super(confFile);
    
        // setup do editor
        myEditor = new GuiEditor(getPosX(), getPosY(), getWidth(), getHeight());
        myEditor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myEditor.setVisible(true);

        // Boas vindas
        greetings();
    }

    @Override
    public void create() throws LWJGLException {
        
        //Display
        Display.setDisplayMode(new DisplayMode(getWidth(), getHeight()));
        Display.setFullscreen(false);
        Display.setTitle("Processamento de malhas poligonais");
        Display.setLocation(getPosX(), getPosY());
        Display.create();

        //Keyboard
        Keyboard.create();

        //ArcBall
        GuiController.create();

        //OpenGL
        initGL();
    }

    @Override
    public void destroy() {
        //Destroi o arcball.
        GuiController.destroy();
        //Destroi o teclado
        Keyboard.destroy();
        //Destroi o render
        Display.destroy();
        //Limpa as configurações
        clearSettings();
       
        //limpa a aplicação e o editor
        myApp    = null;
        myEditor = null;
        canRun   = false;

        //exit
        System.exit(0);
    }

    @Override
    public void run() {
        while (!Display.isCloseRequested() && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {

            if (Display.isVisible()) {
                processKeyboard();
                processMouse();
                render();
            }
            
            Display.update();
        }
    }

    @Override
    public void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
        LightsOn();
        
        //ArcBall
        GuiController.transform();
        
        //MyApp
        if(myApp != null) myApp.render();
        
        if(isShowBox() ) drawBox();
        if(isShowAxis()) drawAxis();
        
        LightsOff();
    }
    
    @Override
    public void reset() {
        //Releitura das configurações
        loadSettings();
        
        // setup do editor
        myEditor = new GuiEditor(getPosX(), getPosY(), getWidth(), getHeight());
        myEditor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myEditor.setVisible(true);

        // Boas vindas
        greetings();
    }
     
    @Override
    public void reload() {
        //releitura das configurações
        loadSettings();
    }
   //-----
    
    public static void greetings() {
        //Boas vindas
        System.out.println("-------------------------------------------------");
        System.out.println("Bem vindo:");
        System.out.println("Aplicativo de processamento de malhas.");
        System.out.println("-------------------------------------------------");
        System.out.println();

        System.out.println("-------------------------------------------------");
        System.out.println("Carregue um projeto, utilizando o menu File-Open.");
        System.out.println("-------------------------------------------------");
        // Cria a aplicação
        System.out.println("Tecla Space: Cria a aplicação.");
        // Executa a aplicação
        System.out.println("Tecla Enter: Executa a aplicação.");
        // Reposiciona observador
        System.out.println("Tecla Down: Releitura das configurações.");
        // Reposiciona observador
        System.out.println("Tecla Up: Reset da visualização.");
        // Reposiciona observador
        System.out.println("Tecla Delete: Deleta a aplicação.");
        // Reposiciona observador
        System.out.println("Tecla Esc: Fecha o aplicativo.");
        System.out.println("-------------------------------------------------");
        System.out.println();

        System.out.println("-------------------------------------------------");
        System.out.println("LOG:");
        System.out.println("-------------------------------------------------");
        System.out.println();    
    }

    //--- Private methods
    
    private boolean initApp() {
        //> App Settings
        String appSettings = myEditor.getCurPath();

        //> Evita erros
        if( appSettings == null ) return false;
        if(    myApp    != null ) myApp = null;
        
        //> Cria a aplicação
        if (appSettings.contains("TriQuad")) {
            myApp = new TriQuadApp(appSettings);
            
        } else {
            System.out.println("Abra um arquivo de configurações primeiro !!");
            return false;
        }
        
        return true;
    }
    
    private void initGL() {
        //Projection
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-1.5, 1.5, -1.5, 1.5, -2.5, 2.5);
        glPushMatrix();

        //Depth test
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
        glClearDepth(100.0);
        glClear(GL_DEPTH_BUFFER_BIT);

        //ModelView
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glPushMatrix();
    }    

    private void processKeyboard() {
        
        // Cria a aplicação
        if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
            try {
                if( initApp() ) {
                    myApp.create();
                    canRun = true;
                }
            } catch (Exception ex) {
                //Libera os recursos
                myApp.destroy();
                canRun = false;
                //LOG
                System.out.println("Erro em Gui::processaKeyboard.");
                ex.printStackTrace();
            } 
        }
        
        // Reset da aplicação
        if(Keyboard.isKeyDown(Keyboard.KEY_BACK)){
            if( myApp != null ) { 
                myApp.reset(); 
                canRun = false;
           }
        }
        
        // Executa a aplicação
        if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
            if( myApp != null && canRun) myApp.run();
        }      
        
        // Reposiciona observador
        if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
            if( myApp != null && canRun) myApp.reload();
        }
        
        // Reposiciona observador
        if(Keyboard.isKeyDown(Keyboard.KEY_UP)) {
            GuiController.reset();
        }
    }

    private void processMouse() {
        GuiController.readInput();
    }

    private void LightsOn() {
        glEnable(GL_LIGHTING);
        glEnable(GL_NORMALIZE);
        glShadeModel(GL_SMOOTH);

        glEnable(GL_LIGHT0);
        glLight(GL_LIGHT0, GL_AMBIENT,  getLight0Amb() );
        glLight(GL_LIGHT0, GL_DIFFUSE,  getLight0Dif() );
        glLight(GL_LIGHT0, GL_SPECULAR, getLight0Esp() );
        glLight(GL_LIGHT0, GL_POSITION, getLight0Pos() );

        glEnable(GL_LIGHT1);
        glLight(GL_LIGHT1, GL_AMBIENT,  getLight1Amb() );
        glLight(GL_LIGHT1, GL_DIFFUSE,  getLight1Dif() );
        glLight(GL_LIGHT1, GL_SPECULAR, getLight1Esp() );
        glLight(GL_LIGHT1, GL_POSITION, getLight1Pos() );

        glEnable(GL_COLOR_MATERIAL);
        glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);

        glMaterialf(GL_FRONT, GL_SHININESS, getMaterialShi() );

        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonOffset(1,1);    
    }

    private void LightsOff() {
        glDisable(GL_COLOR_MATERIAL);
        glDisable(GL_POLYGON_OFFSET_FILL);
        glDisable(GL_LIGHT1);
        glDisable(GL_LIGHT0);
        glDisable(GL_NORMALIZE);
        glDisable(GL_LIGHTING);
        glFlush();    
    }
    
    private void drawAxis() {
        
        glLineWidth(1.5f);
        glColor3d (0.5, 0.5, 0.5);
        glBegin (GL_LINES);
        {
            glColor3d ( 1.0, 0.0, 0.0f );
            glVertex3d(-0.2, 0.0, 0.0f );
            glVertex3d( 0.6, 0.0, 0.0f );

            glColor3d ( 0.0, 1.0, 0.0f );
            glVertex3d( 0.0, 0.6, 0.0f );
            glVertex3d( 0.0,-0.2, 0.0f );


            glColor3d ( 0.0, 0.0, 1.0 );
            glVertex3d( 0.0, 0.0,-0.2f );
            glVertex3d( 0.0, 0.0, 0.6f );
        }
        glEnd (); //GL_LINES
    }
    
    private void drawBox() {

      DoubleBuffer Modelview = BufferUtils.createDoubleBuffer(16);
      glGetDouble(GL_MODELVIEW_MATRIX, Modelview );

      Double [] viewer = new Double[3];
      viewer[0] = Modelview.get(0*4+2);
      viewer[1] = Modelview.get(1*4+2);
      viewer[2] = Modelview.get(2*4+2);

      Double col = 0.0;

      glLineWidth(1);
      glBegin(GL_LINES);
      {
        col = (viewer[1]>0 && viewer[2]>0)? 0.4 : 1.0 ;
        glColor3d(col,0,0);
        glVertex3d(-1.0,-1.0,-1.0); // e0
        glVertex3d( 1.1,-1.0,-1.0); // e1

        col = (viewer[1]<0 && viewer[2]>0)? 0.4 : 1.0 ;
        glColor3d(col,0,0);
        glVertex3d(-1.0, 1.0,-1.0); // e2
        glVertex3d( 1.0, 1.0,-1.0); // e3

        col = (viewer[1]<0 && viewer[2]<0)? 0.4 : 1.0 ;
        glColor3d(col,0,0);
        glVertex3d(-1.0, 1.0, 1.0); // e6
        glVertex3d( 1.0, 1.0, 1.0); // e7

        col = (viewer[1]>0 && viewer[2]<0)? 0.4 : 1.0 ;
        glColor3d(col,0,0);
        glVertex3d(-1.0,-1.0, 1.0); // e4
        glVertex3d( 1.0,-1.0, 1.0); // e5

        /*---------------------------------------------------------------*/

        col = (viewer[0]>0 && viewer[2]>0)? 0.4 : 1.0 ;
        glColor3d(0,col,0);
        glVertex3d(-1.0,-1.0,-1.0); // e0
        glVertex3d(-1.0, 1.1,-1.0); // e2

        col = (viewer[0]<0 && viewer[2]>0)? 0.4 : 1.0 ;
        glColor3d(0,col,0);
        glVertex3d(1,-1,-1); // e1
        glVertex3d(1, 1,-1); // e3

        col = (viewer[0]<0 && viewer[2]<0)? 0.4 : 1.0 ;
        glColor3d(0,col,0);
        glVertex3d(1,-1,1); // e5
        glVertex3d(1, 1,1); // e7

        col = (viewer[0]>0 && viewer[2]<0)? 0.4 : 1.0 ;
        glColor3d(0,col,0);
        glVertex3d(-1,-1,1); // e4
        glVertex3d(-1, 1,1); // e6

        /*---------------------------------------------------------------*/

        col = (viewer[0]>0 && viewer[1]>0)? 0.4 : 1.0 ;
        glColor3d(0,0,col);
        glVertex3d(-1,-1, -1 ); // e0
        glVertex3d(-1,-1,1.1); // e4

        col = (viewer[0]<0 && viewer[1]>0)? 0.4 : 1.0 ;
        glColor3d(0,0,col);
        glVertex3d(1,-1,-1); // e1
        glVertex3d(1,-1, 1); // e5

        col = (viewer[0]<0 && viewer[1]<0)? 0.4 : 1.0 ;
        glColor3d(0,0,col);
        glVertex3d(1,1,-1); // e3
        glVertex3d(1,1, 1); // e7

        col = (viewer[0]>0 && viewer[1]<0)? 0.4 : 1.0 ;
        glColor3d(0,0,col);
        glVertex3d(-1,1,-1); // e2
        glVertex3d(-1,1, 1); // e6
      }
      glEnd();
    }
}
