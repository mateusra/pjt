/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Interface GuiMain
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.apps.gui;

public class GuiMain {

    public static void main(String[] args) {
       GuiApp main = null;        //> interface
       //GUI Settings
       String guiSettings = "./src/ic/apps/gui/GuiApp.settings"; 
  
        try {            
            // Alocação
            main = new GuiApp(guiSettings);
            // Cria a Interface
            main.create();
            // Executa o código
            main.run();
            
        } catch (Exception ex) {
            System.out.println("Erro em GuiMain::main");
            ex.printStackTrace();
        } finally {
            // Libera os recursos
            if (main != null) main.destroy();
        }
    }
}
