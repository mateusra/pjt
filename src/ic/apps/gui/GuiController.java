/**
 * Processamento de malhas poligonais
 * 
 * Pós Graduação IC-UFF 2012.1
 * Classe GuiController
 * 
 * @author Prof. Marcos Lage <mlage@ic.uff.br>
 */

package ic.apps.gui;

//LWJGL
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Quaternion;

//LWJGL estáticos
import static org.lwjgl.opengl.GL11.*;

public class GuiController {
    //Declaração da TrackBall
    private static Quaternion tball; 

    //Ângulos de Euller  
    private static double rotX = 0;
    private static double rotY = 0;
    private static double rotZ = 0;
    
    //Zoom
    private static double zoom = 1;
    
    //Translation
    private static double trsX = 0;
    private static double trsY = 0;
    
    //Sensibilidade
    private static final double ROT_FACTOR =  0.1f;
    private static final double TRS_FACTOR =  0.01f;
    private static final double ZOM_FACTOR =  0.001f;
     
    static void create() throws LWJGLException{
        
      //Mouse
      Mouse.create();
      Mouse.setGrabbed(false);
           
      //Quaternion
      tball = new Quaternion();
       
    }
    
    static void destroy(){
        //Mouse
        Mouse.destroy();
        //Quaternion
        tball = null;
    }
    
    static void readInput() {

        if( Mouse.isButtonDown(0) ){
            tball.set(Mouse.getDY(),Mouse.getDX());
            
            rotX += ROT_FACTOR*tball.x;
            rotY += ROT_FACTOR*tball.y;
            rotZ += ROT_FACTOR*tball.z;       
        } 
        else if( Mouse.isButtonDown(1) ){
            trsX += TRS_FACTOR*Mouse.getDX(); 
            trsY += TRS_FACTOR*Mouse.getDY();         
        }
        else {
            zoom += ZOM_FACTOR*Mouse.getDWheel();
        }
    }
    
    static void transform(){

        //Limpa a matriz de transformação
        glLoadIdentity();
               
        // --- Translação
        glTranslated(trsX, trsY, 0);
        
        // --- Zoom
        glScaled(zoom,zoom,zoom);

        // --- Rotação 
        glRotatef((float)rotX,-1, 0, 0);
        glRotatef((float)rotY, 0, 1, 0);
        glRotatef((float)rotZ, 0, 0, 1);
    }
    
    static void reset(){
        //Rotações
        rotX = rotY = rotZ = 0.0f;
        
        //Translações
        trsX = trsY = 0;
        
        //Escala
        zoom = 1;        
    }
}


